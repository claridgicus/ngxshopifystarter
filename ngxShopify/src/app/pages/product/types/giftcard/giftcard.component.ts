import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router'
import { environment } from '@environment'
import { Product, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'page-giftcard',
	templateUrl: './giftcard.component.html',
	styleUrls: ['./giftcard.component.scss'],
})
export class PageGiftcard implements OnInit {
	@ViewChild('accent') accent: ElementRef
	@Input() product: Product
	environment = environment
	store
	loading: boolean
	imageLoaded: boolean
	optionsMap: any
	optionsObject: any
	availablevariants: any
	variants: any
	objectKeys = Object.keys
	selections: any
	quantity: number
	selectedVariant: any
	variantsMap: any
	selectedVariantSize: boolean = false
	scroll: number
	imagesLoading: boolean
	error: boolean
	_body: any
	form
	currentSlide: number = 0
	_openCareInstructions: boolean = false
	_openMaterialsFabrication: boolean = false
	_openHelp: boolean = false
	constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute, private templateService: TemplateService) {
		this.store = this.templateService.store
		this.loading = true
		this.imagesLoading = true
		this.form = {
			senderEmail: '',
			senderName: '',
			recipientEmail: '',
			recipientName: '',
			message: '',
		}
	}

	ngOnInit() {
		this.processProduct(this.product)
	}

	processProduct(product) {
		window['product'] = product
		this.quantity = 1
		this.imagesLoading = true
		this.loading = true
		this.imageLoaded = false
		this.currentSlide = 0
		this.product = product
		this._body = this.sanitizer.bypassSecurityTrustHtml(this.templateService.unescapeHtml(product.body_html))

		this.loading = false
	}

	updateVariant($event) {
		this.selectedVariant = $event
		this.quantity = 1
	}

	makeSelection(key: string, selection: any) {
		this.variants[key].forEach(element => {
			element.selected = false
		})
		selection.selected = true
		this.selectedVariant = this.product.variants.find(x => {
			return this.variantChecker(x)
		})
	}

	makeHTMLAccordion(string) {
		const words = []
		for (let line of string.split('&lt;/br&gt;')) {
			const textLine = `<div>${line}</div>`
			words.push(textLine)
		}
		return words.join('')
	}

	variantChecker(variant) {
		let result = true
		this.product.options.forEach(option => {
			const selected = this.variants[option.name].find(y => y.selected === true)
			if (variant['option' + option.position] !== selected.variant) {
				result = false
			}
		})
		return result
	}
}
