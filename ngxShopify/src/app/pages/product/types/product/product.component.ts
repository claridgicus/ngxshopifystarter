import { AfterViewChecked, Component, ElementRef, HostListener, Input, OnChanges, ViewChild } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router'
import { listFadeInBasic } from 'app/animations'
import { ngxLightOptions } from 'ngx-light-carousel'
import { Image, Product, TemplateService } from 'ngx-shopify'

@Component({
	animations: [listFadeInBasic],
	selector: 'page-product',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.scss'],
})
export class PageProduct implements OnChanges, AfterViewChecked {
	@ViewChild('accent') accent: ElementRef
	@Input() product: Product
	selectedImage: any
	visible: boolean
	loading: boolean
	recommended: Product[]
	imageLoaded: boolean
	optionsMap: any
	optionsObject: any
	availablevariants: any
	variants: any
	objectKeys = Object.keys
	selections: any
	quantity: number
	selectedVariant: any
	variantsMap: any
	gallery: any
	scroll: number
	direction: string
	imagesLoading: boolean
	error: boolean
	rating: number[]
	reviews: number
	_description: boolean = false
	_body: any
	_shipping: boolean = false
	_shippingDescription: string
	options: ngxLightOptions
	currentSlide: number = 0
	relatedProducts
	metafieldsContent
	hoverSelection: string
	modelWearing: string
	firstImageLoaded: boolean = false
	recommendedLoading: boolean = true

	/* 
		######################
		Lightbox start
		######################
	*/
	lightbox: Image[]
	lightboxOptions: ngxLightOptions
	pageLoaded: boolean = false

	/* 
		######################
		Lightbox end
		######################
	*/

	constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute, private templateService: TemplateService) {
		this.loading = true
		this.imagesLoading = true
		this.options = {
			animation: {
				animationClass: 'transition',
				animationTime: 200,
			},
			swipe: {
				swipeable: true,
				swipeVelocity: 0.005,
			},
			drag: {
				draggable: true,
				dragMany: true,
			},
			scroll: {
				numberToScroll: 1,
			},
			infinite: false,
			autoplay: {
				enabled: false,
				direction: 'right',
				delay: 5000,
				stopOnHover: true,
			},
			breakpoints: [
				{
					width: 9999,
					number: 1.31,
				},
			],
		}
		this.lightboxOptions = {
			...this.options,
			swipe: {
				swipeable: true,
				swipeVelocity: 0.005,
			},
			infinite: false,
			drag: {
				draggable: true,
				dragMany: false,
			},
			breakpoints: [
				{
					width: 9999,
					number: 1,
				},
			],
		}
	}

	ngAfterViewChecked() {
		var ratingValue = document.querySelector('[itemprop="ratingValue"]')
		if (!this.rating && ratingValue) {
			setTimeout(() => {
				this.reviews = parseInt(document.querySelector<HTMLElement>('[itemprop="ratingCount"]').getAttribute('content'))
				this.rating = new Array(parseFloat(document.querySelector<HTMLElement>('[itemprop="ratingValue"]').innerText))
			}, 10)
		}
	}

	ngOnChanges() {
		this.firstImageLoaded = false
		this.imagesLoading = true
		this.imageLoaded = false
		this.visible = false
		this.currentSlide = 0
		this.processProduct(this.product)
		this.getRecommendedProducts(this.product)
	}

	async getRecommendedProducts(product: Product) {
		this.recommended = []
		let recommendedResponse = (await this.templateService.getRecommendationsByProductId(product.id, 4).toPromise()) as any
		for (let i = 0; i < recommendedResponse.products.length; i++) {
			if (localStorage.getItem('product-' + recommendedResponse.products[i].handle)) {
				this.recommended.push(JSON.parse(localStorage.getItem('product-' + recommendedResponse.products[i].handle)))
			} else {
				this.recommended.push(await this.templateService.getProduct(recommendedResponse.products[i].handle, false).toPromise())
			}
		}
		this.recommendedLoading = false
	}

	processProduct(product) {
		this.quantity = 1
		this.selectedImage = product.images[0]
		window['selectedImage'] = this.selectedImage
		this._body = this.sanitizer.bypassSecurityTrustHtml(this.templateService.unescapeHtml(product.body_html))
		this.getRelatedProducts()
		this.getModelWearing()
		this.getMetafieldsContent()
		this.renderVariants()
		this.produceGallery()
		this.produceLightbox()
		this.pageLoaded = true
	}

	async getRelatedProducts() {
		this.relatedProducts = new Array()
		if (this.product.metafields.related.length > 0) {
			let relatedHandles = JSON.parse(this.templateService.unescapeHtml(this.product.metafields.related))
			for (let i = 0; i < relatedHandles.length; i++) {
				this.relatedProducts.push(await this.templateService.getProduct(relatedHandles[i]).toPromise())
			}
		}
	}

	getMetafieldsContent() {
		this._shippingDescription = ''
		if (this.product.metafields.shipping) {
			this._shippingDescription = this.templateService.unescapeHtml(this.product.metafields.shipping)
		}
	}

	getModelWearing() {
		this.modelWearing = ''
		if (this.product.metafields.modelWearing.length) {
			this.modelWearing = this.templateService.unescapeHtml(this.product.metafields.modelWearing)
			// the unescapeHtml function apparently does not remove the ' from a string -- so this function does it for us.
			// simply using the .replace() function doesn't update the actual value, hence why things are x = x.replace
			if (this.modelWearing.indexOf('&#39;') !== -1) {
				this.modelWearing = this.modelWearing.replace('&#39;', "'")
			}
		}
	}

	@HostListener('window:scroll', ['$event'])
	onScroll(event) {
		this.direction = event.srcElement.scrollingElement.scrollTop > this.scroll ? 'down' : 'up'
		this.scroll = event.srcElement.scrollingElement.scrollTop
	}

	showLightbox(i) {
		this.templateService.showLightbox(i)
	}

	updateVariant($event) {
		this.selectedVariant = $event
		this.quantity = 1
		const variantImage = this.product.images.find(x => {
			return x.src.includes(this.selectedVariant.image.src)
		})

		variantImage ? this.selectImage(variantImage) : ''
	}

	returnSelectedImage() {
		return this.selectedImage
	}

	selectImage(image) {
		this.currentSlide = image.position
		this.selectedImage = image
	}

	makeSelection(key: string, selection: any) {
		this.variants[key].forEach(element => {
			element.selected = false
		})
		selection.selected = true
		this.selectedVariant = this.product.variants.find(x => {
			return this.variantChecker(x)
		})
	}

	produceGallery() {
		this.gallery = []
		for (let i = 0; i < this.product.images.length; i++) {
			this.gallery.push(this.product.images[i])
		}
	}

	produceLightbox() {
		this.lightbox = []
		for (let i = 0; i < this.product.images.length; i++) {
			this.lightbox.push(this.product.images[i])
		}
	}

	renderVariants() {
		this.availablevariants = this.product.variants
		this.variants = {}
		this.product.options.forEach(option => {
			this.variants[option.name] = []
		})
		this.availablevariants.forEach(variant => {
			for (let i = 0; i < this.product.options.length; i++) {
				const option = this.product.options[i]
				const variantOption = {
					variant: variant['option' + (i + 1)],
					selected: false,
					available: variant.available,
				}
				this.variants[option.name].push(variantOption)
			}
		})
		this.product.options.forEach(option => {
			this.variants[option.name] = this.variants[option.name].reduce(function(p, c) {
				if (
					!p.some(function(el) {
						return el.variant === c.variant
					})
				) {
					p.push(c)
				}
				return p
			}, [])
		})
		this.product.options.forEach(option => {
			this.variants[option.name][0].selected = true
		})
		this.selectedVariant = this.product.variants.find(x => {
			return this.variantChecker(x)
		})
	}

	variantChecker(variant) {
		let result = true
		this.product.options.forEach(option => {
			const selected = this.variants[option.name].find(y => y.selected === true)
			if (variant['option' + option.position] !== selected.variant) {
				result = false
			}
		})
		return result
	}

	hoverOption(productColor) {
		this.hoverSelection = productColor
	}

	unhoverOption() {
		this.hoverSelection = null
	}

	getShippingDrawer() {
		this.templateService.showDrawer({ type: 'shipping-returns' })
	}
}
