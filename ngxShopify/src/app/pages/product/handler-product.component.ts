import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Product, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'handler-product',
	templateUrl: './handler-product.component.html',
	styleUrls: ['./handler-product.component.scss'],
})
export class HandlerProduct {
	product: Product
	loading: boolean
	constructor(private route: ActivatedRoute, private templateService: TemplateService) {
		this.initProduct()
	}

	async initProduct() {
		this.route.params.subscribe(params => {
			this.loading = true
			if (localStorage.getItem('product-' + params['handle'])) {
				setTimeout(() => {
					this.templateService.getProduct(params['handle'], true).subscribe((product: Product) => {
						localStorage.setItem('product-' + product.handle, JSON.stringify(product))
					})
				}, 30)
				this.product = JSON.parse(localStorage.getItem('product-' + params['handle']))
				this.loading = false
			} else {
				this.templateService.getProductPage(params['handle'], true).subscribe((product: Product) => {
					this.product = product
					window['product'] = localStorage.setItem('product-' + product.handle, JSON.stringify(product))
					this.loading = false
				})
			}
		})
	}
}
