import { Component, OnInit } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'page-collections',
	templateUrl: './collections.component.html',
	styleUrls: ['./collections.component.scss'],
})
export class PageCollections implements OnInit {
	collections: any
	loading: boolean

	constructor(private templateService: TemplateService) {
		this.loading = true
	}

	ngOnInit() {
		this.templateService.getCollectionsPage().subscribe((data: any) => {
			this.collections = data.collections
		})
	}
}
