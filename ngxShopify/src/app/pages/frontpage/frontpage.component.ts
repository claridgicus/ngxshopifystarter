import { Component, OnInit, NgZone, OnDestroy } from '@angular/core'
import { TemplateService } from 'ngx-shopify'
import { environment } from '@environment'
import { Title } from '@angular/platform-browser'
declare const window: any
@Component({
	selector: 'page-front',
	templateUrl: './frontpage.component.html',
	styleUrls: ['./frontpage.component.scss'],
})
export class PageFront implements OnInit, OnDestroy {
	environment = environment
	newFrontpage: any
	loading: boolean
	frontpage
	store

	constructor(private _ngZone: NgZone, private templateService: TemplateService, private titleService: Title) {
		window.angularFrontpage = { component: this, zone: _ngZone }
		this.loading = true
	}

	ngOnInit() {
		this.templateService.frontpage.subscribe(data => {
			this.frontpage = data
			console.log('this.frontpage', this.frontpage)
			this.loading = false
		})
	}

	updateOrder(ids: string[]) {
		this.newFrontpage = []
		for (let i = 0; i < ids.length; i++) {
			this.newFrontpage.push(this.frontpage.find(x => x.id === ids[i]))
		}
		this.frontpage = this.newFrontpage
	}

	updateBlockOnFrontpage(block) {
		let selectedBlock = this.frontpage.find(x => x.id === block.id)
		selectedBlock = block
	}

	ngOnDestroy() {}

	removeFromFrontpage(sectionId) {
		this.frontpage = this.frontpage.filter(x => x.id !== sectionId)
	}

	addToFrontpage(inner) {
		this.frontpage.push(inner)
	}

	updateFrontpage(update) {
		this.frontpage = []
		if (update !== null) {
			this.frontpage = update
		} else {
			this.frontpage.push({
				id: '1553343398794',
				settings: {
					image: '//cdn.shopify.com/s/files/1/0051/1039/9010/files/52119-archer_wallpaper.jpg?v=1553343407',
					alignment: 'center',
					title: 'Image with text overlay',
					text: '\u003cp\u003eUse overlay text to give your customers insight into your brand. Select imagery and text that relates to your style and story.\u003c/p\u003e',
					hero_size: 'medium',
					text_size: 'medium',
					button_label: '',
				},
				type: 'hero',
			})
		}
	}

	load(value) {
		const index = this.frontpage.indexOf(x => x.id !== value.detail.sectionId)
		this.frontpage = this.frontpage.filter(x => x.id !== value.detail.sectionId)
	}
}
