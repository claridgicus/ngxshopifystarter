import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

@Component({
	selector: 'page-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class PageLogin {
	reset: boolean = false
	constructor(private route: ActivatedRoute) {
		this.route.fragment.subscribe((fragment: string) => {
			this.reset = fragment == 'reset' ? true : false
		})
	}
}
