import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Collection, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'handler-collection',
	templateUrl: './handler-collection.component.html',
	styleUrls: ['./handler-collection.component.scss'],
})
export class HandlerCollection {
	collection: Collection
	url: string
	loading: boolean
	constructor(private route: ActivatedRoute, private templateService: TemplateService) {
		this.initCollection()
	}

	async initCollection() {
		this.route.paramMap.subscribe((data: any) => {
			this.loading = true
			this.url = Object.values(data.params).join('/')
			if (this.url) {
				this.templateService.getCollectionPage(this.url, '1', true).subscribe((collection: Collection) => {
					console.log(collection)
					window['collection'] = collection
					this.collection = collection
					setTimeout(() => {
						for (let product of this.collection.products) {
							try {
								localStorage.setItem('product-' + product.handle, JSON.stringify(product))
							} catch {
								console.log('localstorage full handle full state here by deleting older products by their loaded date')
							}
						}
					}, 200)
				})
			} else {
				console.log('no collection')
			}
			this.loading = false
		})
	}
}
