import { AfterViewInit, Component, Input, OnChanges, Renderer2, ViewChild, ViewChildren } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router'
import { Collection, TemplateService } from 'ngx-shopify'
import { interval, Observable, Subject } from 'rxjs'
import { throttle } from 'rxjs/operators'

@Component({
	selector: 'page-collection-with-image',
	templateUrl: './collection-with-image.component.html',
	styleUrls: ['./collection-with-image.component.scss'],
})
export class PageCollectionWithImage implements OnChanges, AfterViewInit {
	@Input() collection: Collection
	@ViewChildren('images') images
	@ViewChildren('products') products
	@ViewChild('endofcollection', { static: true }) endOfCollection
	savedurl: string
	loading: boolean = true
	classes = 'w-md-20'
	pageNumber: number = 1
	scrollAmount: number = 20
	scrollPosition: number = 20
	scrollObserver: IntersectionObserver
	private actionDelay: Subject<any>
	throttleSub: Observable<any>

	constructor(private renderer: Renderer2, private route: ActivatedRoute, private templateService: TemplateService, private titleService: Title) {}

	ngAfterViewInit() {}

	ngOnChanges() {
		if (this.actionDelay) {
			this.actionDelay.unsubscribe()
			this.resetScroll()
		}
		console.log(this.collection)
		this.loading = false
		this.actionDelay = new Subject<any>()
		this.throttleSub = this.actionDelay.asObservable()
		this.throttleSub.pipe(throttle(val => interval(200))).subscribe(data => {
			this.scrollTo(this.scrollPosition + 1)
		})
		this.scrollHandler()
	}

	scrollHandler() {
		this.scrollObserver = new IntersectionObserver(
			result => {
				result.forEach((entry: IntersectionObserverEntry) => {
					if (entry.boundingClientRect.bottom + 20 <= (window.innerHeight || document.documentElement.clientHeight)) {
						this.actionDelay.next({})
					}
				})
			},
			{ threshold: 0.1 }
		)
		this.scrollObserver.observe(this.endOfCollection.nativeElement)
	}

	mergeDeep(destination, source) {
		console.log(destination)
		destination.products = destination.products.concat(source.products)
		// destination = destination.concat(source)
		return destination
	}

	scrollTo(pageNumber: number) {
		if (this.collection && this.collection) {
			if (this.scrollPosition < this.collection.products.length) {
				this.pageNumber = pageNumber
				let results = this.pageNumber * this.scrollAmount
				this.scrollPosition = results
			}
		}
	}

	resetScroll() {
		this.pageNumber = 1
		this.scrollPosition = 0
		this.scrollTo(1)
	}
}
