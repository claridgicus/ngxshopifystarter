import { Component, Input, OnChanges } from '@angular/core'

@Component({
	selector: 'page-blog-standard',
	templateUrl: './blog-standard.component.html',
	styleUrls: ['./blog-standard.component.scss'],
})
export class PageBlogStandard implements OnChanges {
	loading: boolean = true
	@Input() blog

	constructor() {}

	ngOnChanges() {
		console.log('this.blog on blog standard', this.blog)
	}
}
