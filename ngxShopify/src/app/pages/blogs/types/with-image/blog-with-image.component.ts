import { Component, Input, OnChanges } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'page-blog-with-image',
	templateUrl: './blog-with-image.component.html',
	styleUrls: ['./blog-with-image.component.scss'],
})
export class PageBlogWithImage implements OnChanges {
	loading: boolean = true
	@Input() blog

	constructor(private route: ActivatedRoute, private templateService: TemplateService) {}

	ngOnChanges() {}
}
