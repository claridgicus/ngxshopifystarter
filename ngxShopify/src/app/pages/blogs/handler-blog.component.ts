import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'handler-blog',
	templateUrl: './handler-blog.component.html',
	styleUrls: ['./handler-blog.component.scss'],
})
export class HandlerBlog {
	blog: any
	loading: boolean
	constructor(private route: ActivatedRoute, private templateService: TemplateService) {
		this.loading = true
		this.initBlog()
	}

	async initBlog() {
		this.route.params.subscribe(params => {
			this.templateService.getBlogsPage(params.category, '1', true).subscribe(
				(blog: any) => {
					this.blog = blog
					this.loading = false
					console.log('this.blog', this.blog)
				},
				err => {
					console.error('Error!', err)
				}
			)
		})
	}
}
