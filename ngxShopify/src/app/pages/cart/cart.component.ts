import { AfterViewInit, Component, OnInit } from '@angular/core'
import { CartService } from 'ngx-shopify'

@Component({
	selector: 'page-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss'],
})
export class PageCart implements OnInit, AfterViewInit {
	cart: any
	loading: boolean
	constructor(private cartService: CartService) {}

	ngOnInit() {
		this.cartService.cartloading.subscribe((state: boolean) => {
			this.loading = state
		})
		this.cartService.cart.subscribe(
			(cart: any) => {
				this.cart = cart
			},
			err => {
				this.loading = false
			}
		)
		this.cartService.getCart()
	}

	ngAfterViewInit() {
		this.cartService.hideMiniCart()
	}

	back() {
		history.back()
	}
}
