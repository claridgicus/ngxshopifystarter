import { Component, Input, OnChanges } from '@angular/core'

@Component({
	selector: 'page-article-with-image',
	templateUrl: './article-with-image.component.html',
	styleUrls: ['./article-with-image.component.scss'],
})
export class PageArticleWithImage implements OnChanges {
	@Input() article
	content
	constructor() {}

	ngOnChanges() {}
}
