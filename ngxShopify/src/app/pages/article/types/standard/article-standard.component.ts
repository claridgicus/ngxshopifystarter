import { Component, Input, OnChanges } from '@angular/core'

@Component({
	selector: 'page-article-standard',
	templateUrl: './article-standard.component.html',
	styleUrls: ['./article-standard.component.scss'],
})
export class PageArticleStandard implements OnChanges {
	@Input() article
	content
	constructor() {}

	ngOnChanges() {}
}
