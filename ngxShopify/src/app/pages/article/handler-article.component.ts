import { Component } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'handler-article',
	templateUrl: './handler-article.component.html',
	styleUrls: ['./handler-article.component.scss'],
})
export class HandlerArticle {
	article
	loading: boolean = true
	constructor(private route: ActivatedRoute, private templateService: TemplateService, private sanitizer: DomSanitizer) {
		this.loading = true
		this.initArticle()
	}

	async initArticle() {
		this.route.params.subscribe(params => {
			this.templateService.getBlogPage(params.category + '/' + params.post).subscribe(
				(article: any) => {
					console.log('article type:', article.type)
					this.article = article.article
					this.article.content = this.sanitizer.bypassSecurityTrustHtml(this.templateService.unescapeHtml(article.article.content))
					this.loading = false
				},
				err => {
					this.loading = false
				}
			)
		})
	}
}
