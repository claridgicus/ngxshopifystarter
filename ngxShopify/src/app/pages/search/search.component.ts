import { Component, OnInit, Renderer2, ViewChild, AfterViewChecked } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'page-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss'],
})
export class PageSearch implements OnInit, AfterViewChecked {
	@ViewChild('input') input
	searchTerm
	results
	output
	constructor(private route: ActivatedRoute, private templateService: TemplateService, private renderer: Renderer2) {}

	ngOnInit() {
		this.route.queryParams.subscribe(searchTerm => {
			this.searchTerm = searchTerm.search
			this.output = searchTerm.search
			this.runSearch()
		})
	}

	ngAfterViewChecked() {
		this.resize()
	}

	resize() {
		if (this.input && this.input.nativeElement) {
			this.renderer.setStyle(this.input.nativeElement, 'width', this.input.nativeElement.value.length * 24 + 'px')
		}
	}

	runSearch() {
		// this.titleService.setTitle('Search for:' + this.searchTerm + ' - Cookie AU')
		this.templateService.getSearchResults(this.output).subscribe((results: any) => {
			results = results.filter(x => x.image != undefined)
			console.log(results)
			this.results = results
		})
	}

	search() {
		this.templateService.search.emit(this.output)
		this.runSearch()
	}
}
