import { Component, OnInit } from '@angular/core'

@Component({
	selector: 'page-notfound',
	templateUrl: './notfound.component.html',
	styleUrls: ['./notfound.component.scss'],
})
export class PageNotFound implements OnInit {
	constructor() {}

	ngOnInit() {}
}
