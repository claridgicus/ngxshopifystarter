import { Component, Input, OnChanges } from '@angular/core'
import { Page } from 'ngx-shopify'

@Component({
	selector: 'page-landing',
	templateUrl: './landing.component.html',
	styleUrls: ['./landing.component.scss'],
})
export class PageLanding implements OnChanges {
	@Input() page: Page

	ngOnChanges() {}
}
