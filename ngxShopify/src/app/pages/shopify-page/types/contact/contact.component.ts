import { Component, Input, OnChanges } from '@angular/core'
import { Page, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'page-contact',
	templateUrl: './contact.component.html',
	styleUrls: ['./contact.component.scss'],
})
export class PageContact implements OnChanges {
	public dummyElem = document.createElement('div')
	@Input() page: Page
	response: string
	success: boolean
	form: {
		name: string
		phone: string
		email: string
		question: string
	}

	constructor(private templateService: TemplateService) {}

	ngOnChanges() {
		this.form = {
			name: '',
			phone: '',
			email: '',
			question: '',
		}
		window['contactPage'] = this.page
	}

	submit() {
		let form = new FormData()
		form.append('utf8', '✓')
		form.append('form_type', 'contact')
		form.append('contact[name]', this.form.name)
		form.append('contact[phone]', this.form.phone)
		form.append('contact[email]', this.form.email)
		form.append('contact[question]', this.form.question)
		this.templateService.submitForm(form).subscribe(
			(data: any) => {
				this.form = { name: '', phone: '', email: '', question: '' }
				this.success = true
			},
			err => {
				console.log(err)
			}
		)
	}
}
