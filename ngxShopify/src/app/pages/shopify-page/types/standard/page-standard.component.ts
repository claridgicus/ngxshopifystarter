import { Component, Input, OnChanges } from '@angular/core'

@Component({
	selector: 'page-standard',
	templateUrl: './page-standard.component.html',
	styleUrls: ['./page-standard.component.scss'],
})
export class PageStandard implements OnChanges {
	@Input() page: any
	_isUnderlined: string

	ngOnChanges() {
		for (let i = 0; i < this.page.blocksMenu.links.length; i++) {
			if (this.page.title == this.page.blocksMenu.links[i].title) {
				this._isUnderlined = this.page.blocksMenu.links[i].title
			}
		}
	}
}
