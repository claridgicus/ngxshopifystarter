import { Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { environment } from '@environment'

@Component({
	selector: 'page-styleguide',
	templateUrl: './styleguide.component.html',
	styleUrls: ['./styleguide.component.scss'],
})
export class PageStyleGuide {
	@Input() page: any
	openFindInStore: Boolean = false
	currentValue
	constructor(private router: Router) {
		if (environment.production) {
			this.router.navigate(['/'])
		}
		this.currentValue = { value: 'Placeholder' }
	}
}
