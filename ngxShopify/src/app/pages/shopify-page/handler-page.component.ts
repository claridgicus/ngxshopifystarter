import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Page, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'handler-page',
	templateUrl: './handler-page.component.html',
	styleUrls: ['./handler-page.component.scss'],
})
export class HandlerPage {
	page: Page
	loading: boolean

	constructor(private route: ActivatedRoute, private templateService: TemplateService) {
		this.loading = true
		this.initPage()
	}

	async initPage() {
		this.route.params.subscribe(data => {
			this.loading = true
			this.page = new Page()
			this.templateService.getPage(data['page'], true).subscribe((page: Page) => {
				this.page = page
				if (this.page.content) {
					this.page.content = this.templateService.unescapeHtml(this.page.content)
				}
				this.loading = false
			})
		})
	}
}
