export enum ImageSize {
	minicart = '88x132',
	tiny = '100x100',
	thumb = '250x340',
	small = '500x500',
	medium = '1000x1000',
	large = '1500x1500',
	slider = '800x350',
	portraitHero = '500x700',
	landscapeHero = '1500x450',
}

export enum ImageCrop {
	center = 'crop_center',
}
