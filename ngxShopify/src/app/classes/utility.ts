import { SafeResourceUrl } from '@angular/platform-browser'

export class Video {
	videoWebm: string
	videoMp4: string
	youtube: string
	webmSafe: SafeResourceUrl
	mp4Safe: SafeResourceUrl
	youtubeSafe: SafeResourceUrl
	mute: boolean
	autoplay: boolean
}

export class Image {
	alt: string
	src: string
	retina: string
	height?: number
	width?: number
}

export class Menu {}

export class MenuItem {}
