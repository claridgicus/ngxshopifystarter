import { Component, OnInit } from '@angular/core'

@Component({
	selector: 'layout-empty',
	templateUrl: './empty.component.html',
	styleUrls: ['./empty.component.scss'],
})
export class LayoutEmpty implements OnInit {
	constructor() {}

	ngOnInit() {}
}
