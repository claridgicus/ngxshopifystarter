import { Component, OnInit } from '@angular/core'
import { NavigationEnd, NavigationError, NavigationStart, Router, RouterOutlet } from '@angular/router'

@Component({
	selector: 'layout-page',
	templateUrl: './page.component.html',
	styleUrls: ['./page.component.scss'],
})
export class LayoutPage implements OnInit {
	loading: boolean = false
	constructor(private router: Router) {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationStart) {
				this.loading = true
				// Show loading indicator
			}

			if (event instanceof NavigationEnd) {
				this.loading = false
				// Hide loading indicator
			}

			if (event instanceof NavigationError) {
				// Hide loading indicator
				this.loading = false
				// Present error to user
			}
		})
	}

	ngOnInit() {}

	prepareRoute(outlet: RouterOutlet) {
		return outlet && outlet.activatedRouteData
	}
}
