import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { FrontPagePage } from './frontpage.component'

describe('MenuComponent', () => {
	let component: FrontPagePage
	let fixture: ComponentFixture<FrontPagePage>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [FrontPagePage],
		}).compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(FrontPagePage)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})
})
