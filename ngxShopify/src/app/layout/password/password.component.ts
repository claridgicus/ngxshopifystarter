import { Component } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'layout-password',
	templateUrl: './password.component.html',
	styleUrls: ['./password.component.scss'],
})
export class LayoutPassword {
	store

	constructor(private templateService: TemplateService) {
		this.store = this.templateService.store
	}
}
