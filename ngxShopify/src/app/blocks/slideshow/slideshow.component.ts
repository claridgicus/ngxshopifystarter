import { Component, OnInit, Input } from '@angular/core'
import { ngxLightOptions } from 'ngx-light-carousel'

@Component({
	selector: 'block-slideshow',
	templateUrl: './slideshow.component.html',
	styleUrls: ['./slideshow.component.scss'],
})
export class BlockSlideshow implements OnInit {
	@Input() block: any
	options: ngxLightOptions = {
		animation: {
			animationClass: 'transition', // done
			animationTime: 200,
		},
		swipe: {
			swipeable: true, // done
			swipeVelocity: 0.005, // done - check amount
		},
		scroll: {
			numberToScroll: 1,
		},
		drag: {
			draggable: true, // done
			dragMany: true,
		},
		infinite: true,
		autoplay: {
			enabled: true,
			direction: 'right',
			delay: 5000,
			stopOnHover: true,
		},
		breakpoints: [
			{
				width: 9999,
				number: 1,
			},
		],
	}
	constructor() {}

	ngOnInit() {}
}
