import { Component, OnInit, Input, ViewChild, Renderer2, AfterViewChecked, ViewChildren, QueryList, HostListener, AfterContentChecked } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ngxLightOptions } from 'ngx-light-carousel'

@Component({
	selector: 'block-content-header',
	templateUrl: './content-header.component.html',
	styleUrls: ['./content-header.component.scss'],
})
export class BlockContentHeader implements OnInit, AfterViewChecked, AfterContentChecked {
	@ViewChildren('slides') slides
	@ViewChild('hero', { static: true }) hero
	@ViewChild('carou', { static: true }) carou
	@ViewChild('video') video
	@Input() block: any
	blocks: any
	current: number = 0
	settings: any
	products
	fullscreenOpen: any
	options: ngxLightOptions
	constructor(private renderer: Renderer2, private sanitiser: DomSanitizer) {
		this.options = {
			animation: {
				animationClass: 'transition', // done
				animationTime: 200,
			},
			swipe: {
				swipeable: true, // done
				swipeVelocity: 0.005, // done - check amount
			},
			scroll: {
				numberToScroll: 1,
			},
			drag: {
				draggable: true, // done
				dragMany: true,
			},
			infinite: true,
			autoplay: {
				enabled: true,
				direction: 'right',
				delay: 5000,
				stopOnHover: true,
			},
			breakpoints: [
				{
					width: 9999,
					number: 1,
				},
			],
		}
	}

	ngOnInit() {
		this.current = 0
		this.settings = this.block.settings
		this.blocks = this.block.blocks
		if (this.block.settings.video) {
			const autoplay = this.block.settings.autoplay ? '&autoplay=1' : ''
			const mute = this.block.settings.mute ? '&mute=1' : ''
			this.block.settings.video = this.sanitiser.bypassSecurityTrustResourceUrl(this.block.settings.video + '?controls=0' + autoplay + mute)
		}
	}

	ngAfterViewChecked() {
		this.matchSize()
	}

	ngAfterContentChecked() {
		this.matchSize()
	}

	matchSize() {
		if (this.hero && this.slides) {
			const slides = this.slides.toArray()
			var dotsElement: HTMLElement = document.querySelector('block-content-header .indicators')
			if (window.innerWidth < 768) {
				slides.forEach(x => {
					this.renderer.setStyle(x.nativeElement, 'height', 'inherit')
					this.renderer.setStyle(dotsElement, 'top', slides[0].nativeElement.children[1].offsetHeight + 15 + 'px')
				})
			} else {
				slides.forEach(x => {
					this.renderer.setStyle(x.nativeElement, 'height', `${this.hero.nativeElement.offsetHeight}px`)
				})
				this.renderer.setStyle(dotsElement, 'top', this.hero.nativeElement.children[0].offsetHeight + 15 + 'px')
			}
		}
	}

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.matchSize()
	}

	fullscreen() {
		// Trigger fullscreen
		if (!this.fullscreenOpen) {
			const docElmWithBrowsersFullScreenFunctions = this.video.nativeElement as HTMLElement & {
				mozRequestFullScreen(): Promise<void>
				webkitRequestFullscreen(): Promise<void>
				msRequestFullscreen(): Promise<void>
			}

			if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
				docElmWithBrowsersFullScreenFunctions.requestFullscreen()
			} else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) {
				/* Firefox */
				docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen()
			} else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) {
				/* Chrome, Safari and Opera */
				docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen()
			} else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) {
				/* IE/Edge */
				docElmWithBrowsersFullScreenFunctions.msRequestFullscreen()
			}
			this.fullscreenOpen = true
		} else {
			const docWithBrowsersExitFunctions = document as Document & {
				mozCancelFullScreen(): Promise<void>
				webkitExitFullscreen(): Promise<void>
				msExitFullscreen(): Promise<void>
			}
			if (docWithBrowsersExitFunctions.exitFullscreen) {
				docWithBrowsersExitFunctions.exitFullscreen()
			} else if (docWithBrowsersExitFunctions.mozCancelFullScreen) {
				/* Firefox */
				docWithBrowsersExitFunctions.mozCancelFullScreen()
			} else if (docWithBrowsersExitFunctions.webkitExitFullscreen) {
				/* Chrome, Safari and Opera */
				docWithBrowsersExitFunctions.webkitExitFullscreen()
			} else if (docWithBrowsersExitFunctions.msExitFullscreen) {
				/* IE/Edge */
				docWithBrowsersExitFunctions.msExitFullscreen()
			}
			this.fullscreenOpen = false
		}
	}
}
