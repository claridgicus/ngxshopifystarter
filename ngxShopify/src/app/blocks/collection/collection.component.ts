import { Component, OnInit, Input } from '@angular/core'
import { Product } from 'ngx-shopify'

@Component({
	selector: 'block-collection',
	templateUrl: './collection.component.html',
	styleUrls: ['./collection.component.scss'],
})
export class BlockCollection implements OnInit {
	@Input() block: any
	collection: any
	constructor() {}

	ngOnInit() {
		this.collection = this.block
	}
}

export class FeaturedCollection {
	settings: any
	collection: any
	products: Product[]
	type: string
}
