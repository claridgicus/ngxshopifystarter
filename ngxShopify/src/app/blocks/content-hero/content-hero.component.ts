import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-content-hero',
	templateUrl: './content-hero.component.html',
	styleUrls: ['./content-hero.component.scss'],
})
export class BlockContentHero implements OnInit {
	@Input() block: any

	constructor() {}

	ngOnInit() {}
}
