import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-twocolumn-hero',
	templateUrl: './twocolumn-hero.component.html',
	styleUrls: ['./twocolumn-hero.component.scss'],
})
export class BlockTwoColumnHero implements OnInit {
	@Input() block: any
	constructor() {}

	ngOnInit() {}
}
