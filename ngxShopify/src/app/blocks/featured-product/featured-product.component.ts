import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core'
import { Product } from 'ngx-shopify'

@Component({
	selector: 'block-featured-product',
	templateUrl: './featured-product.component.html',
	styleUrls: ['./featured-product.component.scss'],
})
export class BlockFeaturedProduct implements OnInit {
	@ViewChild('accent') accent: ElementRef
	@Input() block: any
	product: Product
	metafields: any
	images: any
	direction: string
	scroll: number
	loading: boolean = true
	selectedVariant: any
	quantity: number = 1
	constructor() {}

	ngOnInit() {
		this.images = this.block.images
		this.product = this.block.product
		this.selectedVariant = this.product.variants[0]
	}
}
