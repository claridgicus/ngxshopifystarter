import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-feature-row',
	templateUrl: './feature-row.component.html',
	styleUrls: ['./feature-row.component.scss'],
})
export class BlockFeatureRow implements OnInit {
	@Input() block: any

	constructor() {}

	ngOnInit() {}
}
