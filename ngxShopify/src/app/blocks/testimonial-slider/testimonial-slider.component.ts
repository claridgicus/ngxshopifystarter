import { Component, OnInit, Input } from '@angular/core'
import { ngxLightOptions } from 'ngx-light-carousel'

@Component({
	selector: 'block-testimonial-slider',
	templateUrl: './testimonial-slider.component.html',
	styleUrls: ['./testimonial-slider.component.scss'],
})
export class BlockTestimonialSlider implements OnInit {
	@Input() block: any
	blocks
	options: ngxLightOptions
	constructor() {
		this.blocks = []
		this.options = {
			animation: {
				animationClass: 'transition', // done
				animationTime: 200,
			},
			swipe: {
				swipeable: true, // done
				swipeVelocity: 0.005, // done - check amount
			},
			drag: {
				draggable: true, // done
				dragMany: true,
			},
			scroll: {
				numberToScroll: 1,
			},
			infinite: false,
			autoplay: {
				enabled: true,
				direction: 'right',
				delay: 5000,
				stopOnHover: true,
			},
			breakpoints: [
				{
					width: 768,
					number: 1,
				},
				{
					width: 991,
					number: 3,
				},
				{
					width: 9999,
					number: 3,
				},
			],
		}
	}

	ngOnInit() {
		this.blocks = this.block.blocks
	}
}
