import { Component, Input } from '@angular/core'

@Component({
	selector: 'block-featured-content',
	templateUrl: './featured-content.component.html',
	styleUrls: ['./featured-content.component.scss'],
})
export class BlockFeaturedContent {
	@Input() block: any
}
