import { Component, OnInit, Input } from '@angular/core'
import { ngxLightOptions } from 'ngx-light-carousel'

@Component({
	selector: 'block-icon-slider',
	templateUrl: './icon-slider.component.html',
	styleUrls: ['./icon-slider.component.scss'],
})
export class BlockIconSlider {
	@Input() block: any
	options: ngxLightOptions

	constructor() {
		this.options = {
			animation: {
				animationClass: 'transition', // done
				animationTime: 200,
			},
			swipe: {
				swipeable: true, // done
				swipeVelocity: 0.005, // done - check amount
			},
			drag: {
				draggable: true, // done
				dragMany: true,
			},
			infinite: true,
			autoplay: {
				enabled: true,
				direction: 'right',
				delay: 5000,
				stopOnHover: true,
			},
			scroll: {
				numberToScroll: 1,
			},
			breakpoints: [
				{
					width: 639,
					number: 1,
				},
				{
					width: 768,
					number: 3,
				},
				{
					width: 991,
					number: 4,
				},
				{
					width: 9999,
					number: 5,
				},
			],
		}
	}
}
