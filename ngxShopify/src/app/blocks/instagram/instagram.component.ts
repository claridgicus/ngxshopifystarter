import { Component, OnInit, Input } from '@angular/core'
import { TemplateService } from 'ngx-shopify'
import { environment } from '@environment'
@Component({
	selector: 'instagram',
	templateUrl: './instagram.component.html',
	styleUrls: ['./instagram.component.scss'],
})
export class BlockInstagram implements OnInit {
	@Input() block: any
	pics: any
	key
	today = new Date()
	constructor(private templateService: TemplateService) {}

	ngOnInit() {
		this.key = this.templateService.store.header.instagram ? this.templateService.store.header.instagram : environment.instagram
		if (!localStorage.getItem('instagram')) {
			this.getInsta()
		} else {
			this.instafeed()
		}
	}

	instafeed() {
		this.pics = JSON.parse(localStorage.getItem('instagram')).data
		if (this.pics.expiry > new Date()) {
			this.getInsta()
		}
	}

	getInsta() {
		if (this.key) {
			this.templateService.getInstagramFeed(this.key).subscribe((data: any) => {
				data.expiry = this.today.setHours(this.today.getHours() + 4)
				localStorage.setItem('instagram', JSON.stringify(data))
				this.instafeed()
			})
		}
	}
}
