import { Component, Input } from '@angular/core'

@Component({
	selector: 'block-link-list',
	templateUrl: './link-list.component.html',
	styleUrls: ['./link-list.component.scss'],
})
export class BlockLinkList {
	@Input() block: any

	constructor() {}
}
