import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-ribbon',
	templateUrl: './ribbon.component.html',
	styleUrls: ['./ribbon.component.scss'],
})
export class BlockRibbon implements OnInit {
	@Input() block: any
	constructor() {}

	ngOnInit() {}
}
