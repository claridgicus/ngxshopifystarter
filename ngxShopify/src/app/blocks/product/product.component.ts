import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-product',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.scss'],
})
export class BlockProduct implements OnInit {
	@Input() block: any
	product: any
	constructor() {}

	ngOnInit() {
		this.product = this.block.product
	}
}
