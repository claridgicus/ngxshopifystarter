import { Component, Input, OnInit } from '@angular/core'

@Component({
	selector: 'block-collection-list',
	templateUrl: './collection-list.component.html',
	styleUrls: ['./collection-list.component.scss'],
})
export class BlockCollectionList implements OnInit {
	@Input() block: any

	constructor() {}

	ngOnInit() {}
}

export class CollectionList {
	id: string
	settings: {
		title: string
		displayImages: boolean
	}
	blocks: [
		{
			title: string
			image: {
				alt: string
				src: string
			}
			link: string
		}
	]
	type: string
}
