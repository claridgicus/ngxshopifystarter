import { Component, OnInit, Input } from '@angular/core'
@Component({
	selector: 'block-gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.scss'],
})
export class BlockGallery implements OnInit {
	@Input() block: any
	constructor() {}

	ngOnInit() {}
}
