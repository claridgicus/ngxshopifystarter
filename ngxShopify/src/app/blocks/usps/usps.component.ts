import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-usps',
	templateUrl: './usps.component.html',
	styleUrls: ['./usps.component.scss'],
})
export class BlockUsps implements OnInit {
	@Input() block: any

	constructor() {}

	ngOnInit() {}
}
