import { Component, OnInit, Input } from '@angular/core'
import { ngxLightOptions } from 'ngx-light-carousel'

@Component({
	selector: 'block-collection-slider',
	templateUrl: './collection-slider.component.html',
	styleUrls: ['./collection-slider.component.scss'],
})
export class BlockCollectionSlider implements OnInit {
	@Input() block: any
	options: ngxLightOptions
	products

	constructor() {
		this.products = []
		this.options = {
			animation: {
				animationClass: 'transition',
				animationTime: 200,
			},
			swipe: {
				swipeable: true,
				swipeVelocity: 0.005,
			},
			drag: {
				draggable: true,
				dragMany: true,
			},
			scroll: {
				numberToScroll: 1,
			},
			infinite: true,
			autoplay: {
				enabled: true,
				direction: 'right',
				delay: 5000,
				stopOnHover: true,
			},
			breakpoints: [
				{
					width: 640,
					number: 1,
				},
				{
					width: 991,
					number: 3,
				},
				{
					width: 9999,
					number: 4,
				},
			],
		}
	}

	ngOnInit() {
		this.products = this.block.products.slice(0, 12)
	}
}
