import { Component, HostListener, Input } from '@angular/core'

@Component({
	selector: 'block-hero',
	templateUrl: './hero.component.html',
	styleUrls: ['./hero.component.scss'],
})
export class BlockHero {
	@Input() block: any
	viewportSize: number = 0
	constructor() {
		this.onResize(window.innerWidth)
	}

	@HostListener('window:resize', ['$event.target.innerWidth'])
	onResize(innerWidth) {
		this.viewportSize = innerWidth
	}
}
