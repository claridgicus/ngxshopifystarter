import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-video',
	templateUrl: './video.component.html',
	styleUrls: ['./video.component.scss'],
})
export class BlockVideo implements OnInit {
	@Input() block: any

	constructor() {}

	ngOnInit() {}
}
