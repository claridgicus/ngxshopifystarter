import { Component, OnInit, Input } from '@angular/core'

@Component({
	selector: 'block-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.scss'],
})
export class BlockBlog implements OnInit {
	@Input() block: any

	constructor() {}

	ngOnInit() {}
}
