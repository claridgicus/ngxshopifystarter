import { animate, state, style, transition, trigger } from '@angular/animations'

export const openClose = trigger('openClose', [
	state(
		'*',
		style({
			height: '*',
			pointerEvents: 'all',
			opacity: '*',
			paddingTop: '*',
			paddingBottom: '*',
			overflow: 'hidden',
		})
	),
	state(
		'void',
		style({
			height: '0px',
			pointerEvents: 'none',
			opacity: '0',
			paddingTop: '0',
			paddingBottom: '0',
			overflow: 'hidden',
		})
	),
	transition(':enter', [animate('0.15s ease-in-out')]),
	transition(':leave', [animate('0.15s ease-in-out')]),
])

// trigger('Fading', [state('void', style({ opacity: 0 })), state('*', style({ opacity: 1 })), transition(':enter', animate('800ms ease-out')), transition(':leave', animate('800ms ease-in'))])
