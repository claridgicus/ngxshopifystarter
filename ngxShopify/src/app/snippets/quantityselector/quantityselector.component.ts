import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core'
import { CartService } from 'ngx-shopify'

@Component({
	selector: 'quantityselector',
	templateUrl: './quantityselector.component.html',
	styleUrls: ['./quantityselector.component.scss'],
})
export class SnippetQuantitySelector implements OnInit, OnChanges {
	@Input() cartItem: any = null
	@Input() variant: any = null
	@Output() update = new EventEmitter<number>()
	quantity: number
	max: number
	loading: boolean
	constructor(private cartService: CartService) {}

	ngOnInit() {
		this.max = this.variant.inventory_quantity ? this.variant.inventory_quantity : 10
		this.quantity = this.cartItem ? this.cartItem.quantity : 1
	}

	ngOnChanges() {
		if (this.variant) {
			this.loading = false
			this.max = this.variant.inventory_quantity ? this.variant.inventory_quantity : 10
			this.quantity = this.cartItem ? this.cartItem.quantity : 1
		} else {
			// this.max = this.variant.inventory_quantity
			this.quantity = this.cartItem ? this.cartItem.quantity : 1
			this.loading = true
		}
	}

	changeQtyUp() {
		this.cartItem ? this.changeCartQty(this.cartItem.quantity + 1) : this.changeProductQty(this.quantity + 1)
	}

	changeQtyDown() {
		this.cartItem ? this.changeCartQty(this.cartItem.quantity - 1) : this.changeProductQty(this.quantity - 1)
	}

	async changeCartQty(quantity: number) {
		this.loading = true
		await this.cartService.updateQuantity(this.cartItem.variant_id, quantity)
		this.loading = false
	}

	async changeProductQty(quantity: number) {
		this.quantity = quantity
		this.update.emit(this.quantity)
	}
}
