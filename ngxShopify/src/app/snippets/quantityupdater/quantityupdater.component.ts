import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core'
import { CartService } from 'ngx-shopify'

@Component({
	selector: 'quantityupdater',
	templateUrl: './quantityupdater.component.html',
	styleUrls: ['./quantityupdater.component.scss'],
})
export class SnippetQuantityUpdater implements OnInit {
	@Input() cartItem: any
	loading: boolean
	constructor(private cartService: CartService) {}

	ngOnInit() {}

	changeQtyUp() {
		this.changeQty(this.cartItem.quantity + 1)
	}

	changeQtyDown() {
		this.changeQty(this.cartItem.quantity - 1)
	}

	async changeQty(quantity: number) {
		this.loading = true
		await this.cartService.updateQuantity(this.cartItem.variant_id, quantity)
		this.loading = false
	}
}
