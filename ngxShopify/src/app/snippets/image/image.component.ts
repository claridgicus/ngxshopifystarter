import { animate, state, style, transition, trigger } from '@angular/animations'
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core'
import { ImageSize } from '@classes'
import { environment } from '@environment'
import { Image } from 'ngx-shopify'

@Component({
	selector: 'ngxImg',
	templateUrl: './image.component.html',
	styleUrls: ['./image.component.scss'],
	animations: [trigger('Fading', [state('void', style({ opacity: 0 })), state('*', style({ opacity: 1 })), transition(':enter', animate('150ms ease-out')), transition(':leave', animate('150ms ease-in'))])],
})
export class SnippetImage implements OnChanges, AfterViewInit {
	@ViewChild('image') element
	@ViewChild('loader') loader
	@Input() loading?: boolean = true
	@Input() image?: Image
	@Input() src?: string
	@Input() crop: string
	@Input() classString?: string
	@Input() altString?: string
	@Input() size?: { height: number; width: number }
	@Input() reload?: boolean
	@Input() cover?: boolean
	@Output() load = new EventEmitter<boolean>()
	@Input() sizeSelect?: string
	@Input() process?: boolean = true
	@Input() default?: string
	srcset: string
	srcResult
	retinaResult
	imageSize = ImageSize
	observer: IntersectionObserver
	inView: boolean = false
	constructor(private el: ElementRef) {}

	ngOnChanges() {
		this.calc()
	}

	updateUrl(event) {
		event.preventDefault()
		event.stopPropagation()
		if (event.type == 'error') {
			if (event.srcElement.src.includes(this.src)) {
				delete this.image
				delete this.crop
				delete this.sizeSelect
				delete this.size
				this.inView = true
				this.loading = false
				this.process = false
				this.src = environment.defaultImg
				this.calc()
				this.srcset = this.src
			}
		}
	}

	ngAfterViewInit() {
		this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage()
		if (this.src || this.image) {
			this.calc()
		}
	}

	private canLazyLoad() {
		return window && 'IntersectionObserver' in window
	}

	private lazyLoadImage() {
		const obs = new IntersectionObserver(entries => {
			entries.forEach(({ isIntersecting }) => {
				if (isIntersecting) {
					this.loadImage()
					obs.unobserve(this.el.nativeElement)
				}
			})
		})
		obs.observe(this.el.nativeElement)
	}

	private loadImage() {
		this.inView = true
	}

	processUrl(string) {
		if (this.process) {
			if (this.image) {
				return string.replace('1x1', this.imageSize[this.sizeSelect])
			} else {
				return this.replaceLast(string, '.', '_' + this.imageSize[this.sizeSelect] + '.')
			}
		} else {
			return string
		}
	}

	processRetinaUrl(string) {
		if (this.process) {
			if (this.image) {
				string = string.replace('1x1', this.imageSize[this.sizeSelect])
				return this.replaceLast(string, '.', '@2x.')
			} else {
				return this.replaceLast(string, '.', '@2x.')
			}
		} else {
			return string
		}
	}

	processCropUrl(string) {
		if (this.crop) {
			return this.replaceLast(string, '.', '_crop_' + this.crop + '.')
		}
		return string
	}

	calc() {
		if (this.reload) {
			this.src = ''
		}
		this.srcset = ''
		if (this.image) {
			this.src = this.src ? this.src : this.image.src
			this.altString = this.altString ? this.altString : this.image.alt
		}
		this.altString = this.altString ? this.altString : ''
		this.srcResult = this.processCropUrl(this.processUrl(this.src))
		if (this.sizeSelect) {
			this.size = {
				height: this.imageSize[this.sizeSelect].split('x')[1],
				width: this.imageSize[this.sizeSelect].split('x')[0],
			}
		}
		this.retinaResult = this.processRetinaUrl(this.srcResult)
		this.srcset = this.srcResult + ' 1x, ' + this.retinaResult + ' 2x'
	}

	loadEmit() {
		this.loading = false
		this.load.emit(true)
	}

	replaceLast(string: string, substring: string, replacement: string) {
		if (string == undefined) {
			return string
		}
		let index: number = string.lastIndexOf(substring)
		if (index == -1) return string
		return string.substring(0, index) + replacement + string.substring(index + substring.length)
	}
}
