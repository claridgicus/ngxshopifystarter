import { Component, OnInit, Input, OnChanges } from '@angular/core'
import { CartService } from 'ngx-shopify'
import { Product } from 'ngx-shopify'
import { Router } from '@angular/router'

@Component({
	selector: 'addtocart',
	templateUrl: './addtocart.component.html',
	styleUrls: ['./addtocart.component.scss'],
})
export class SnippetAddtocart implements OnInit {
	@Input() variant: any
	@Input() quantity: number
	@Input() classes?: string
	@Input() product?: Product
	@Input() navigateTo?: string
	available: boolean
	string: string
	processing: string

	constructor(private cartService: CartService, private router: Router) {
		this.processing = 'first'
	}

	ngOnInit() {
		this.classes = this.classes === undefined ? '' : this.classes
		this.string = 'Add to cart'
	}

	async addToCart(event) {
		event.target.blur()
		this.string = 'Adding to cart'
		this.processing = 'second'
		const product = {
			id: this.variant.id,
			quantity: this.quantity,
		}
		await this.cartService.addToCart(product)
		if (this.navigateTo) {
			this.router.navigate([this.navigateTo])
		}
		this.processing = 'third'
		this.string = 'Added to cart'
	}
}
