import { Component, OnInit, Renderer2 } from '@angular/core'
import { Router } from '@angular/router'
import { openClose } from 'app/animations'
import { TemplateService } from 'ngx-shopify'

@Component({
	animations: [openClose],
	selector: 'snippet-mobile-menu',
	templateUrl: './mobilemenu.component.html',
	styleUrls: ['./mobilemenu.component.scss'],
})
export class SnippetMobileMenu implements OnInit {
	showmenu: boolean
	showsubmenu: boolean
	header
	store
	searchString: string
	grandChildLinks: any
	grandChildTitle: string
	parentTitle: string
	menuPrimary
	menuSecondary
	constructor(private templateService: TemplateService, private renderer: Renderer2, private router: Router) {
		this.showmenu = false
		this.showsubmenu = false
		this.store = this.templateService.store
		this.header = this.store.header
		this.grandChildLinks = null
		this.grandChildTitle = null
	}

	ngOnInit() {
		this.templateService.menu.subscribe((state: boolean) => {
			this.renderer.setStyle(document.body, 'overflow', 'visible')
			this.showmenu = state
		})
		// pseudo-deepclone of the header objects
		this.menuPrimary = JSON.parse(JSON.stringify(this.templateService.processMenu(this.templateService.store.header.links)))
		this.menuSecondary = JSON.parse(JSON.stringify(this.templateService.processMenu(this.templateService.store.header.mobilelinks)))
		window['menuPrimary'] = this.menuPrimary
		window['menuSecondary'] = this.menuSecondary
	}

	toggleMenu() {
		this.showmenu = !this.showmenu
		this.templateService.toggleMenu(this.showmenu)
		if (this.showMenu) {
			this.renderer.setStyle(document.body, 'overflow', 'hidden')
		} else {
			this.renderer.setStyle(document.body, 'overflow', 'visible')
		}
	}

	showMenu() {
		this.templateService.toggleMenu(true)
		this.renderer.setStyle(document.body, 'overflow', 'hidden')
	}

	hideMenu() {
		this.templateService.toggleMenu(false)
		this.renderer.setStyle(document.body, 'overflow', 'visible')
	}

	showSubMenu(childTitle, links, parentTitle) {
		this.showsubmenu = true
		this.grandChildTitle = childTitle
		this.parentTitle = parentTitle
		this.grandChildLinks = this.templateService.processMenu(links)
	}

	hideSubMenu() {
		this.showsubmenu = false
		this.grandChildTitle = null
		this.grandChildLinks = null
	}

	submitSearch() {
		this.router.navigate(['search/'], {
			queryParams: { search: this.searchString },
		})
		this.hideMenu()
	}

	handleClick() {
		this.hideMenu()
	}
}
