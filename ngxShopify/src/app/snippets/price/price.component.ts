import { Component, OnInit, Input, OnChanges } from '@angular/core'

@Component({
	selector: 'price',
	templateUrl: './price.component.html',
	styleUrls: ['./price.component.scss'],
})
export class SnippetPrice implements OnChanges {
	@Input() quantity: any
	@Input() variant: any
	@Input() afterpay?: boolean
	@Input() showFullPrice?: boolean = true
	price: number

	constructor() {
		if (this.variant) {
			this.price = this.variant.price
		}
	}

	ngOnChanges() {
		if (this.variant) {
			this.price = this.variant.price
		}
	}

	calculatedPrice() {
		return (this.quantity * this.price) / 100
	}
}
