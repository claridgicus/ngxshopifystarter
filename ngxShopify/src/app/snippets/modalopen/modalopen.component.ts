import { Component, Input } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'modal-open',
	templateUrl: './modalopen.component.html',
	styleUrls: ['./modalopen.component.scss'],
})
export class SnippetModalOpen {
	@Input() page: string
	constructor(private templateService: TemplateService) {}

	showDrawer() {
		console.log('hit')
		this.templateService.showDrawer({
			type: 'page',
			meta: {
				page: this.page,
			},
		})
	}
}
