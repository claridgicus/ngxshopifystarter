import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Product, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'variantselector',
	templateUrl: './variantselector.component.html',
	styleUrls: ['./variantselector.component.scss'],
})
export class SnippetVariantSelector implements OnInit {
	@Input() product: Product
	@Input() option: any
	@Output() selectedVariant = new EventEmitter<any>()
	loading: boolean

	constructor(private route: ActivatedRoute, private templateService: TemplateService) {}

	ngOnInit() {
		this.loading = true
		window['product'] = this.product
		this.route.queryParams.subscribe(params => {
			this.option = this.option ? this.option : {}
			for (let i = 0; i < this.product.options_with_values.length; i++) {
				this.option['option' + (i + 1)] = null
			}
			if (params.variant) {
				this.selectVariantById(params.variant)
			} else {
				this.selectVariantById(this.product.selected_or_first_available_variant.id)
			}
			this.loading = false
		})
		this.loading = false
	}

	isActive(value, option) {
		if (this.option['option' + option.position] === value) {
			return true
		}
	}

	isColor(optionName) {
		if (optionName.toLowerCase() == 'color' || optionName.toLowerCase() == 'colour' || optionName.toLowerCase() == 'colors' || optionName.toLowerCase() == 'colours') {
			return true
		}
	}

	findSelectedOption(i) {
		if (this.option['option' + i] !== null) {
			return this.option['option' + i]
		} else {
			return ' Please Select '
		}
	}

	selectVariantById(id) {
		let selectedVariant = this.product.variants.find(x => x.id == id)
		if (!selectedVariant) {
			return
		}
		this.option = {
			...(selectedVariant.option1 && { option1: selectedVariant.option1 }),
			...(selectedVariant.option2 && { option2: selectedVariant.option2 }),
			...(selectedVariant.option3 && { option3: selectedVariant.option3 }),
		}
		this.selectedVariant.emit(selectedVariant)
	}

	selectOption(value, option) {
		let optionModified = this.product.options_with_values.find(x => x.name == option.name)
		this.option['option' + optionModified.position] = value
		let selected = this.calculateVariant()
		this.selectedVariant.emit(selected)
	}

	hoverOption(value, option) {
		option.hoverSelection = value
	}

	unhoverOption(option) {
		option.hoverSelection = null
	}

	calculateVariant() {
		let selected = this.product.variants.find(x => {
			if (this.option.option1 !== undefined) {
				if (this.option.option1 == x.option1) {
					if (this.option.option2 !== undefined) {
						if (this.option.option2 == x.option2) {
							if (this.option.option3 !== undefined) {
								if (this.option.option3 == x.option3) {
									return true
								}
							} else {
								return true
							}
						}
					} else {
						return true
					}
				}
			} else {
				return true
			}
			return false
		})
		return selected
	}

	openSizeModal() {
		let sizeGuideModal = {
			type: 'sizeguide',
			meta: {
				position: 'center',
				isActive: this.product.tags.includes('collection-footwear') ? 'footwear' : 'apparel',
			},
		}
		this.templateService.showModal(sizeGuideModal)
	}
}
