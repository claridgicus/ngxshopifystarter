import { Component, OnInit } from '@angular/core'
import { CartService } from 'ngx-shopify'
import { environment } from '@environment'
import { Router } from '@angular/router'

@Component({
	selector: 'cartcount',
	templateUrl: './cartcount.component.html',
	styleUrls: ['./cartcount.component.scss'],
})
export class SnippetCartcount implements OnInit {
	i: any
	dot = environment.cartCount.dot
	counter = environment.cartCount.counter

	constructor(private cartService: CartService, private router: Router) {
		this.i = ''
		this.cartService.getCart()
	}

	ngOnInit() {
		this.cartService.cart.subscribe((cart: any) => (this.i = cart && cart.item_count ? cart.item_count : '0'))
	}

	showCart() {
		if (environment.minicart) {
			this.cartService.showMiniCart()
		} else {
			this.router.navigate(['/cart'])
		}
	}
}
