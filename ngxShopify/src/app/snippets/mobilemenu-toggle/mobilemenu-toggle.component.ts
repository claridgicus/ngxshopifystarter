import { Component } from '@angular/core'
import { StoreService } from '@services'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'mobile-menu-toggle',
	templateUrl: './mobilemenu-toggle.component.html',
	styleUrls: ['./mobilemenu-toggle.component.scss'],
})
export class SnippetMobileMenuToggle {
	menuOpen: boolean

	constructor(private storeService: StoreService, private templateService: TemplateService) {
		this.templateService.menu.subscribe(data => {
			this.menuOpen = data
		})
	}

	showMenu() {
		// this.storeService.closeAllMenus()
		this.templateService.toggleMenu(true)
	}

	hideMenu() {
		this.templateService.toggleMenu(false)
	}

	toggleMenu() {
		this.menuOpen = !this.menuOpen
		if (this.menuOpen) {
			this.showMenu()
		} else {
			this.hideMenu()
		}
	}
}
