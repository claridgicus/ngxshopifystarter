import { Component, Input, AfterViewInit, OnChanges, ViewEncapsulation } from '@angular/core'
import { LifeCycleService } from 'ngx-shopify'

@Component({
	selector: 'okendo',
	templateUrl: './okendo.component.html',
	styleUrls: ['./okendo.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class SnippetOkendo implements AfterViewInit, OnChanges {
	@Input() productId: any
	reviewsWidget
	constructor(private lifecycleService: LifeCycleService) {
		let script = document.createElement('script')
		script.type = 'application/json'
		script.async = true
		script.id = 'oke-reviews-settings'
		script.charset = 'utf-8'
		script.innerHTML = '{"subscriberId": "1ec34ba6-5bcc-4cb7-8506-0f7f27207988"}'
		document.getElementsByTagName('head')[0].appendChild(script)
	}

	ngOnChanges() {
		this.load()
	}

	ngAfterViewInit() {
		this.load()
	}

	async load() {
		const script = await this.lifecycleService.load('okendo')
		this.reviewsWidget = window.document.querySelector('#oke-reviews-widget')
		if (this.reviewsWidget) {
			window['okeReviewsWidgetOnInit'] = function(okendoInitApi) {
				window['okendoInitApi'] = okendoInitApi
			}
			if (window['okendoInitApi']) {
				window['okendoInitApi'].initReviewsWidget(this.reviewsWidget)
			}
		}
	}
}
