import { animate, state, style, transition, trigger } from '@angular/animations'
import { Component, HostListener, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { TemplateService } from 'ngx-shopify'
@Component({
	animations: [
		trigger('openClose', [
			state(
				'open',
				style({
					width: '200px',
					marginLeft: '20px',
				})
			),
			state(
				'closed',
				style({
					width: '0px',
					marginLeft: '0px',
				})
			),
			transition('open => closed', [animate('150ms')]),
			transition('closed => open', [animate('150ms')]),
		]),
	],
	selector: 'searchinput',
	templateUrl: './searchinput.component.html',
	styleUrls: ['./searchinput.component.scss'],
})
export class SnippetSearchInput {
	@ViewChild('searchInput', { static: true }) searchInput
	searchOpen: boolean = false
	searchString: string

	constructor(private templateService: TemplateService, private router: Router) {}

	searchEmit() {
		this.templateService.search.emit(this.searchString)
	}

	showSearch() {
		this.searchOpen = !this.searchOpen
		this.searchString = null
		setTimeout(() => {
			// this will make the execution after the above boolean has changed
			this.searchInput.nativeElement.focus()
		}, 1)
	}

	hideSearch() {
		this.searchOpen = false
		this.searchString = null
	}

	search() {
		if (this.searchString.length > 3) {
			this.searchEmit()
		}
		this.router.navigate(['search/'], { queryParams: { search: this.searchString } })
	}

	@HostListener('document:click')
	clickedOutside() {
		this.hideSearch()
	}
}
