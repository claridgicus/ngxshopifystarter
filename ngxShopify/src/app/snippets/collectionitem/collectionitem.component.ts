import { Component, Input } from '@angular/core'
import { CartService } from 'ngx-shopify'
import { AddToCart, Product } from 'ngx-shopify'
import { Router } from '@angular/router'

@Component({
	selector: 'collectionitem',
	templateUrl: './collectionitem.component.html',
	styleUrls: ['./collectionitem.component.scss'],
})
export class SnippetCollectionItem {
	@Input() product: Product
	constructor(private router: Router, private cartService: CartService) {}

	button(e) {
		e.stopPropagation()
		e.preventDefault()
	}

	addToCart(product) {
		const addToCart: AddToCart = {
			quantity: 1,
			id: product.selected_or_first_available_variant.id,
		}
		this.cartService.addToCart(addToCart)
		this.router.navigate(['/cart'])
	}
}
