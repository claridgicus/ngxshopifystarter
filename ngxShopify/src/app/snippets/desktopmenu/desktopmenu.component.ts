import { animate, state, style, transition, trigger } from '@angular/animations'
import { Component, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { TemplateService } from 'ngx-shopify'
@Component({
	animations: [
		trigger('openClose', [
			state(
				'true',
				style({
					height: '*',
					pointerEvents: 'all',
					opacity: '*',
				})
			),
			state(
				'false',
				style({
					height: '0px',
					pointerEvents: 'none',
					opacity: '0',
				})
			),
			transition('true => false', [animate('0.1s ease-in-out')]),
			transition('false => true', [animate('0.1s ease-in-out')]),
		]),
	],
	selector: 'desktop-menu',
	templateUrl: './desktopmenu.component.html',
	styleUrls: ['./desktopmenu.component.scss'],
})
export class SnippetDesktopMenu implements OnInit {
	@ViewChild('menu', { static: true }) menu
	open: boolean = false
	header
	selectedImage: any

	constructor(private renderer: Renderer2, private router: Router, private templateService: TemplateService) {
		this.header = this.templateService.store.header
		this.menuClose()
		this.router.events.subscribe(event => {
			this.menuClose()
		})
	}

	clickedInside($event: Event) {
		$event.preventDefault()
		$event.stopPropagation()
	}

	@HostListener('document:click')
	clickedOutside() {
		this.menuClose()
	}

	ngOnInit() {
		this.header.links = this.templateService.processMenu(this.header.links)
	}

	menuClose() {
		this.open = false
		this.header.links.forEach(link => {
			link.active = false
			if (link.levels > 0) {
				link.links.forEach(link => {
					link.active = false
					if (link.levels > 0) {
						link.links.forEach(link => {
							link.active = false
						})
					}
				})
			}
		})
	}

	menuOpen(link) {
		this.menuClose()
		this.open = true
		link.active = true
	}

	@HostListener('window:resize', ['$event'])
	onResize($event) {
		if ($event.target.innerWidth < 991) {
			this.menuClose()
		}
	}
}
