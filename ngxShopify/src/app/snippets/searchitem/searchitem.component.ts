import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { AddToCart, CartService } from 'ngx-shopify'

@Component({
	selector: 'searchitem',
	templateUrl: './searchitem.component.html',
	styleUrls: ['./searchitem.component.scss'],
})
export class SnippetSearchItem implements OnInit {
	@Input() searchItem
	constructor(private cartService: CartService, private router: Router) {}

	ngOnInit() {
		const replace = this.searchItem.tags.find((x: string) => x.includes('Subtitle_'))
		this.searchItem.subtitle = replace ? replace.replace('Subtitle_') : ''
		console.log(this.searchItem)
	}

	button(e) {
		e.stopPropagation()
		e.preventDefault()
	}

	addToBag(variant) {
		const addToCart: AddToCart = {
			quantity: 1,
			id: variant.id,
		}
		this.cartService.addToCart(addToCart)
		this.router.navigate(['/cart'])
	}
}
