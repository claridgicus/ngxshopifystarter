import { Component, Input, OnInit } from '@angular/core'
import { Video } from 'ngx-shopify'
import { DomSanitizer } from '@angular/platform-browser'

@Component({
	selector: 'video-player',
	templateUrl: './video.component.html',
	styleUrls: ['./video.component.scss'],
})
export class SnippetVideo implements OnInit {
	@Input() video: Video

	constructor(private sanitiser: DomSanitizer) {}

	ngOnInit() {
		console.log(this.video)
		this.video.mp4Safe = this.sanitiser.bypassSecurityTrustResourceUrl(this.video.videoMp4 as string)
		this.video.webmSafe = this.sanitiser.bypassSecurityTrustResourceUrl(this.video.videoWebm as string)
		this.video.youtubeSafe = this.sanitiser.bypassSecurityTrustResourceUrl(this.video.youtube as string)
	}
}
