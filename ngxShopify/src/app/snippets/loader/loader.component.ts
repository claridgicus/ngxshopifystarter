import { Component, Input, OnInit } from '@angular/core'

@Component({
	selector: 'loader',
	templateUrl: './loader.component.html',
	styleUrls: ['./loader.component.scss'],
})
export class SnippetLoader implements OnInit {
	@Input() loading: boolean

	constructor() {}

	ngOnInit() {}
}
