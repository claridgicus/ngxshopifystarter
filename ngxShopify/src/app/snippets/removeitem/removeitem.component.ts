import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core'
import { CartService } from 'ngx-shopify'

@Component({
	selector: 'removeitem',
	templateUrl: './removeitem.component.html',
	styleUrls: ['./removeitem.component.scss'],
})
export class SnippetRemoveItem implements OnInit {
	@Input() cartItem: any
	loading: boolean
	constructor(private cartService: CartService) {}

	ngOnInit() {}

	remove() {
		this.cartService.updateQuantity(this.cartItem.id, 0)
	}
}
