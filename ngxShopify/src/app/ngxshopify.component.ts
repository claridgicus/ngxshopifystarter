import { Component } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/mergeMap'
import { LifeCycleService } from 'ngx-shopify'

@Component({
	selector: 'angular-shopify',
	template: '<router-outlet></router-outlet>',
})
export class NgxShopify {
	constructor(private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute, private lifecycleService: LifeCycleService) {
		this.router.events
			.filter(event => event instanceof NavigationEnd)
			.map(() => this.activatedRoute)
			.map(route => {
				while (route.firstChild) route = route.firstChild
				return route
			})
			.filter(route => route.outlet === 'primary')
			.mergeMap(route => route.data)
			.subscribe(event => this.titleService.setTitle(event['title']))
	}
}
