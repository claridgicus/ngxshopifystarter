import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from 'ngx-shopify'
import { ComponentAccountHeader, ComponentActivate, ComponentAddresses, ComponentDashboard, ComponentOrder, ComponentOrders, ComponentRegister, ComponentReset, ComponentsAccountInformation } from './sections'
import { LayoutEmpty, LayoutPage, LayoutPassword } from './layout'
import { HandlerArticle, HandlerBlog, HandlerCollection, HandlerPage, HandlerProduct, PageAccount, PageCart, PageCollections, PageFront, PageLogin, PageNotFound, PageSearch, PageStyleGuide } from './pages'

const routes: Routes = [
	{ path: 'password', component: LayoutPassword },
	{
		path: '',
		component: LayoutPage,
		children: [
			{ path: '', component: PageFront },
			{
				path: 'collections',
				component: LayoutEmpty,
				children: [
					{ path: '', component: PageCollections },
					{ path: 'null', component: PageCollections },
					{
						path: ':name',
						component: LayoutEmpty,
						children: [
							{ path: '', component: HandlerCollection },
							{ path: 'products/:handle', component: HandlerProduct },
							{
								path: ':name2',
								component: LayoutEmpty,
								children: [
									{
										path: '',
										component: HandlerCollection,
									},
									{
										path: 'products/:handle',
										component: HandlerProduct,
									},
									{
										path: ':name3',
										component: HandlerCollection,
									},
								],
							},
						],
					},
				],
			},
			{
				path: 'account',
				component: PageAccount,
				children: [
					{ path: 'register', component: ComponentRegister },
					{
						path: 'login',
						component: PageLogin,
					},
					{
						path: 'activate/:userId/:token',
						component: ComponentActivate,
					},
					{
						path: 'reset/:userId/:token',
						component: ComponentReset,
					},
					{
						path: '',
						canActivate: [AuthGuard],
						component: ComponentAccountHeader,
						children: [
							{
								path: 'addresses',
								component: ComponentAddresses,
								canActivate: [AuthGuard],
							},
							{
								path: 'orders',
								component: ComponentOrders,
								canActivate: [AuthGuard],
							},
							{
								path: 'orders/:id',
								component: ComponentOrder,
								canActivate: [AuthGuard],
							},
							{
								path: 'info',
								component: ComponentsAccountInformation,
								canActivate: [AuthGuard],
							},
							{
								path: '',
								component: ComponentDashboard,
								canActivate: [AuthGuard],
							},
						],
					},
				],
			},
			{ path: 'cart', component: PageCart, data: { title: 'Cart' } },
			{ path: 'pages/:page', component: HandlerPage },
			{ path: 'search', component: PageSearch },
			{ path: 'products/:handle', component: HandlerProduct },
			{ path: 'products/null', component: HandlerProduct },
			{
				path: 'blogs/:category',
				component: LayoutEmpty,
				children: [
					{
						path: '',
						component: HandlerBlog,
					},
					{
						path: ':post',
						component: HandlerArticle,
					},
				],
			},
			{ path: 'styleguide', component: PageStyleGuide },
			{ path: '**', component: PageNotFound },
		],
	},
]
@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			paramsInheritanceStrategy: 'always',
			scrollPositionRestoration: 'enabled',
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
