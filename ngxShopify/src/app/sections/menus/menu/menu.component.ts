import { Component, ElementRef, HostListener, ViewChildren } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss'],
})
export class ComponentMenu {
	store

	constructor(private templateService: TemplateService) {
		this.store = this.templateService.store
		console.log('this.store:', this.store)
	}

	@ViewChildren('link')
	menus: ElementRef

	clickedInside($event: Event) {
		$event.preventDefault()
		$event.stopPropagation()
	}

	@HostListener('document:click', ['$event'])
	clickedOutside($event) {
		this.menuClose()
	}

	menuClose() {
		this.store.header.links.forEach(link => {
			link.active = false
		})
	}

	menuOpen(link) {
		this.menuClose()
		link.active = true
	}
}
