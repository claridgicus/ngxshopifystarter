import { Component, OnInit, HostListener, ElementRef } from '@angular/core'
import { CartService, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-search-overlay',
	templateUrl: './searchoverlay.component.html',
	styleUrls: ['./searchoverlay.component.scss'],
})
export class ComponentSearchOverlay implements OnInit {
	display: boolean = false
	constructor(private templateService: TemplateService) {}

	ngOnInit() {
		this.templateService.search.subscribe(searchTerm => {
			this.display = searchTerm.length > 3 ? true : false
		})
	}
}
