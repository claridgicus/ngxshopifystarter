import { Component, Input, OnInit } from '@angular/core'

@Component({
	selector: 'component-ribbon',
	templateUrl: './ribbon.component.html',
	styleUrls: ['./ribbon.component.scss'],
})
export class ComponentRibbon implements OnInit {
	@Input() ribbon: {
		settings: RibbonSettings
		blocks: any
	}

	ngOnInit() {}
}

export class RibbonSettings {
	ribbon: boolean
	text: string
	colorBackground: string
	colorText: string
}
