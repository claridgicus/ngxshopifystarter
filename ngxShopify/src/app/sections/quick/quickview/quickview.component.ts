import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { Product } from 'ngx-shopify'

@Component({
	selector: 'quickview',
	templateUrl: './quickview.component.html',
	styleUrls: ['./quickview.component.scss'],
})
export class ComponentQuickView implements OnInit {
	@Input() product: Product
	@Output() dismissed = new EventEmitter<boolean>()

	constructor() {}

	ngOnInit() {}

	dismiss() {
		this.dismissed.emit(true)
	}
}
