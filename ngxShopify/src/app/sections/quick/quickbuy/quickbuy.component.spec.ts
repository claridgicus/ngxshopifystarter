import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { QuickbuyComponent } from './quickbuy.component'

describe('QuickbuyComponent', () => {
	let component: QuickbuyComponent
	let fixture: ComponentFixture<QuickbuyComponent>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [QuickbuyComponent],
		}).compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(QuickbuyComponent)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})
})
