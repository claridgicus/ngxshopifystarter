import { Component, HostListener } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class ComponentHeader {
	stuck: boolean
	store: any
	ribbon: any

	constructor(private templateService: TemplateService) {
		this.stuck = false
		this.ribbon = this.templateService.store.ribbon
		this.store = this.templateService.store
	}

	@HostListener('window:scroll')
	@HostListener('window:resize')
	scroll() {
		if (window.innerWidth > 640) {
			if (Math.round(window.scrollY) >= 36) {
				this.stuck = true
			} else if (Math.round(window.scrollY) < 37) {
				this.stuck = false
			}
		} else {
			this.stuck = true
		}
	}
}
