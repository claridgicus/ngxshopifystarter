import { Component, OnInit, Renderer2 } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-drawer',
	templateUrl: './drawer.component.html',
	styleUrls: ['./drawer.component.scss'],
})
export class ComponentDrawer implements OnInit {
	drawer: any
	constructor(private templateService: TemplateService, private renderer: Renderer2) {}

	ngOnInit() {
		this.templateService.drawer.subscribe(state => {
			if (state === null || state.type === 'close') {
				this.renderer.removeClass(document.body, 'overflow-hidden')
				delete this.drawer
			} else {
				this.templateService.getPage(state.type).subscribe((data: any) => {
					this.renderer.addClass(document.body, 'overflow-hidden')
					this.drawer = data
					this.drawer.content = this.templateService.unescapeHtml(this.drawer.content)
				})
			}
		})
	}

	hideDrawer() {
		this.templateService.hideDrawer()
	}
}
