import { DOCUMENT } from '@angular/common'
import { Component, Inject, Input, OnInit, Renderer2 } from '@angular/core'
import { ngxLightOptions } from 'ngx-light-carousel'
import { TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-lightbox',
	templateUrl: './lightbox.component.html',
	styleUrls: ['./lightbox.component.scss'],
})
export class ComponentLightbox implements OnInit {
	@Input() images: any
	@Input() options: ngxLightOptions
	@Input() classString: string
	extraClass: string
	lightbox: any
	currentIndex: number
	overlayClass: string = ''
	constructor(@Inject(DOCUMENT) private document: Document, private templateService: TemplateService, private renderer: Renderer2) {}

	ngOnInit() {
		this.calculateRatio()

		this.templateService.lightbox.subscribe((state: string) => {
			if (state === null) {
				delete this.lightbox
				this.renderer.removeClass(this.document.documentElement, 'overflow-hidden')
			} else {
				this.renderer.addClass(this.document.documentElement, 'overflow-hidden')
				this.currentIndex = parseInt(state)
				this.lightbox = true
			}
		})
	}

	hideLightbox() {
		this.templateService.hideLightbox()
	}

	calculateRatio() {
		const imgRatio = 0.6
		const windowRatio = window.innerWidth / window.innerHeight
		if (windowRatio < imgRatio) {
			this.extraClass = 'd-flex align-items-center'
		} else {
			this.extraClass = ''
		}
	}

	clickInside($event) {
		$event.preventDefault()
		$event.stopPropagation()
	}
}
