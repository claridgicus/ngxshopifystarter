import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ComponentFooter } from './header.component'

describe('ComponentFooter', () => {
	let component: ComponentFooter
	let fixture: ComponentFixture<ComponentFooter>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ComponentFooter],
		}).compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(ComponentFooter)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})
})
