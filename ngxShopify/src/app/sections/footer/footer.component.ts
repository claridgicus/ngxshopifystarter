import { animate, state, style, transition, trigger } from '@angular/animations'
import { Component, HostListener } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
	animations: [
		trigger('openClose', [
			state(
				'true',
				style({
					height: '*',
					pointerEvents: 'all',
					opacity: '*',
				})
			),
			state(
				'false',
				style({
					height: '0px',
					pointerEvents: 'none',
					opacity: '0',
				})
			),
			transition('true => false', [animate('0.1s ease-in-out')]),
			transition('false => true', [animate('0.1s ease-in-out')]),
		]),
	],
	selector: 'component-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss'],
})
export class ComponentFooter {
	footer: any
	store: any

	constructor(private templateService: TemplateService) {
		this.store = this.templateService.store
		this.footer = this.store.footer
		this.footer.blocks.forEach(block => {
			if (block.menu) {
				block.links = this.templateService.processMenu(block.links)
			}
		})
		this.menuReset()
	}

	@HostListener('window:resize', ['$event'])
	onResize($event) {
		if ($event.target.innerWidth > 991) {
			this.menuReset()
		}
	}

	menuReset() {
		this.footer.blocks.forEach(block => {
			block.open = false
		})
	}
}
