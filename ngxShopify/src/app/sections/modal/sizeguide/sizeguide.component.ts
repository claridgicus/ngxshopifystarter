import { Component, Input } from '@angular/core'
import { TemplateService } from 'ngx-shopify'

@Component({
  selector: 'modal-sizeguide',
  templateUrl: './sizeguide.component.html',
  styleUrls: ['./sizeguide.component.scss'],
})
export class ModalSizeguide {
  constructor(private templateService: TemplateService) {}
  @Input() isActive: string = 'apparel'

  close() {
    this.templateService.hideModal()
  }
}
