import { Component, OnInit, Renderer2 } from '@angular/core'
import { Modal, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-modal',
	templateUrl: './modal.component.html',
	styleUrls: ['./modal.component.scss'],
})
export class ComponentModal implements OnInit {
	modal: any
	constructor(private templateService: TemplateService, private renderer: Renderer2) {}

	ngOnInit() {
		this.templateService.modal.subscribe((state: Modal) => {
			if (state.type === 'close') {
				this.renderer.removeClass(document.body, 'overflow-hidden')
				delete this.modal
			} else {
				this.modal = state
				this.renderer.addClass(document.body, 'overflow-hidden')
			}
		})
	}

	hideModal() {
		this.templateService.hideModal()
	}

	clickedInside(event: Event) {
		event.stopPropagation()
		event.preventDefault()
	}
}
