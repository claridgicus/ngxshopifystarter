import { Component } from '@angular/core'
import { Router } from '@angular/router'
import gql from 'graphql-tag'
import { AccountService, StorefrontService, TemplateService } from 'ngx-shopify'

@Component({
  selector: 'component-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class ComponentDashboard {
  response: any
  error: any
  errorMessage: string
  data
  store
  loader
  constructor(private storefrontService: StorefrontService, private accountService: AccountService, private router: Router, templateService: TemplateService) {
    this.store = templateService.store
    this.loader = this.store.shop.cmLoader
    this.getCustomerData()
    this.storefrontService
      .runGraphqlMutation(
        gql`
          mutation {
            checkoutCreate(input: { lineItems: [] }) {
              checkout {
                id
                webUrl
                lineItems(first: 5) {
                  edges {
                    node {
                      title
                      quantity
                    }
                  }
                }
              }
              checkoutUserErrors {
                code
                field
                message
              }
            }
          }
        `,
        {}
      )
      .subscribe((data: any) => {
        // window.location = data.data.checkoutCreate.checkout.webUrl

        this.storefrontService
          .runGraphqlMutation(
            gql`
              mutation checkoutCustomerAssociateV2($checkoutId: ID!, $customerAccessToken: String!) {
                checkoutCustomerAssociateV2(checkoutId: $checkoutId, customerAccessToken: $customerAccessToken) {
                  checkout {
                    id
                    webUrl
                  }
                  checkoutUserErrors {
                    code
                    field
                    message
                  }
                  customer {
                    id
                  }
                }
              }
            `,
            { checkoutId: data.data.checkoutCreate.checkout.id, customerAccessToken: this.storefrontService.customerAccessToken() }
          )
          .subscribe(data => {})
      })
  }



  async getCustomerData() {
    this.data = await this.storefrontService.customerDetails().toPromise()
  }

  logout() {
    this.accountService.logout()
    this.router.navigate(['/'])
  }
}
