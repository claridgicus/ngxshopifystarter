import { Component, Renderer2, ViewChild } from '@angular/core'
import { environment } from '@environment'
import { MailingAddress, MailingAddressInput, StorefrontService, TemplateService } from 'ngx-shopify'
import { Subscription } from 'rxjs'

@Component({
	selector: 'component-addresses',
	templateUrl: './addresses.component.html',
	styleUrls: ['./addresses.component.scss'],
})
export class ComponentAddresses {
	@ViewChild('selectCountry') selectCountry
	customer: any
	address: MailingAddressInput
	addresses: MailingAddress[]
	loading: boolean
	editAddress: MailingAddress
	countries: any[]
	provinces: string[]
	default: boolean = false
	newAddress: boolean = false
	subscriptionCustomerDetails: Subscription

	store
	loader
	constructor(private storefrontService: StorefrontService, private templateService: TemplateService, private renderer: Renderer2) {
		this.setup()
		this.loading = true
		this.store = templateService.store
		this.addresses = []
		this.address = new MailingAddressInput()
		this.countries = environment.allowedAddresses.map(x => {
			return { value: x.readable }
		})
	}

	setup() {
		this.subscriptionCustomerDetails = this.storefrontService.customerDetails().subscribe((response: any) => {
			this.addresses = response.data.customer.addresses.edges.map(x => x.node)
			this.customer = response.data.customer
			window['customer'] = this.customer
			this.loading = false
		})
	}

	selectAddress(address) {
		this.editAddress = address
	}

	async deleteAddress(address) {
		this.storefrontService.customerAddressDelete(address.id).toPromise()
		this.addresses = this.addresses.filter(x => x.id !== address.id)
	}

	createAddress() {
		this.editAddress = new MailingAddress()
		this.newAddress = true
	}

	cancel() {
		this.editAddress = null
		this.newAddress = false
	}

	selectCountryEvent(value) {
		const country = environment.allowedAddresses.find(x => x.readable == value)
		if (country && country.provinces && country.provinces.length > 0) {
			this.provinces = country.provinces.map(x => {
				return x[0]
			})
			this.editAddress.province = this.provinces[0]
		} else {
			this.editAddress.province = ''
			this.provinces = null
		}
	}

	async updateAddress(address: MailingAddress) {
		let result
		let newAddress = new MailingAddressInput()
		newAddress.address1 = address.address1
		newAddress.address2 = address.address2
		newAddress.city = address.city
		newAddress.company = address.company
		newAddress.country = address.country
		newAddress.firstName = address.firstName
		newAddress.lastName = address.lastName
		newAddress.phone = address.phone
		newAddress.province = address.province
		newAddress.zip = address.zip

		if (this.newAddress) {
			// One can only update a default address. We require an address.id to update default address, which we don't have when we create a new address.
			// So, when we have a new address that is to be the default, we need to await the returned ID of the new address, then make a second call to update to default.
			result = await this.storefrontService.customerAddressCreate(newAddress).toPromise()
			if (result && this.default) {
				await this.storefrontService.customerDefaultAddressUpdate(result.data.customerAddressCreate.customerAddress.id).toPromise()
			}
		} else {
			result = await this.storefrontService.customerAddressUpdate(address.id, newAddress).toPromise()
			if (this.default) {
				await this.storefrontService.customerDefaultAddressUpdate(address.id).toPromise()
			}
		}

		this.loading = true
		this.editAddress = null
		this.newAddress = false
		this.customer = null
		this.subscriptionCustomerDetails.unsubscribe()
		this.setup()
	}
}
