import { Component } from '@angular/core'
import { AccountService, StorefrontService, TemplateService } from 'ngx-shopify'

@Component({
  selector: 'component-account-information',
  templateUrl: './account-information.component.html',
  styleUrls: ['./account-information.component.scss'],
})
export class ComponentsAccountInformation {
  data
  store
  loader
  
  constructor(private storefrontService: StorefrontService, private accountService: AccountService, templateService: TemplateService) {
    this.store = templateService.store
    this.loader = this.store.shop.cmLoader
    this.getCustomerData()
  }

  async getCustomerData() {
    this.data = await this.storefrontService.customerDetails().toPromise()
  }
}
