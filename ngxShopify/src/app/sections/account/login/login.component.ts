import { HttpClient } from '@angular/common/http'
import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { environment } from '@environment'
import { AccountService, CustomerAccessTokenCreateInput, LifeCycleService, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class ComponentLogin {
	response: any
	customer: CustomerAccessTokenCreateInput
	error: string
	invalid: boolean = false
	store: any
	environment = environment
	constructor(private accountService: AccountService, private route: ActivatedRoute, private lifecycleService: LifeCycleService, private router: Router, private templateService: TemplateService, private http: HttpClient) {
		this.store = this.templateService.store
		console.log('login store', this.store)
		this.store.shop.loginText = decodeURIComponent(this.store.shop.loginText).replace(/\+/g, ' ')
		this.route.params.subscribe(params => {
			if (this.accountService.userTokenPresent) {
				this.router.navigate([params.return_url])
			}
		})
		this.response = ''
		this.customer = { email: '', password: '' }
	}

	dataChanged(e) {
		this.error = ''
		this.invalid = false
	}

	async submit() {
		// if(this.environment.production){
		const form = new FormData()
		form.append('utf8', '✓')
		form.append('form_type', 'customer_login')
		form.append('customer[email]', this.customer.email)
		form.append('customer[password]', this.customer.password)
		const headers = this.lifecycleService.ngxHttpRequestNoErrorPage(this.lifecycleService.ngxHttpRequestHeaders('text', false))
		const result = await this.http
			.post(environment.apiurl + 'account/login', form, { headers: headers })
			.toPromise()
			.then(() => {
				this.execute()
			})
			.catch(() => {
				this.execute()
			})
	}

	async execute() {
		await this.accountService
			.login(this.customer, '/account')
			.then(data => {})
			.catch(err => {
				// TODO - handle when a customer doesnt have an account or has the wrong password here
				if (err.length) {
					this.invalid = true
					this.error = err[0].message
				}
			})
	}
}
