import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { CustomerActivateByUrlInput, StorefrontService } from 'ngx-shopify'

@Component({
  selector: 'component-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.scss'],
})
export class ComponentActivate {
  customer: CustomerActivateByUrlInput = new CustomerActivateByUrlInput()
  constructor(private storefrontService: StorefrontService, private router: Router) {}
  // todo
  async submit() {
    this.storefrontService.customerActivateByUrl({ activationUrl: window.location.href, password: this.customer.password }).subscribe(data => {
      this.router.navigate(['/'])
    })
  }
}
