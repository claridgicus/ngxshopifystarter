import { Component } from '@angular/core'
import { CustomerRecoverInput, StorefrontService } from 'ngx-shopify'

@Component({
	selector: 'component-reset-request',
	templateUrl: './reset-request.component.html',
	styleUrls: ['./reset-request.component.scss'],
})
export class ComponentResetRequest {
	customer: CustomerRecoverInput = { email: '' }
	success: boolean
	error
	constructor(private storefrontService: StorefrontService) {}

	async submit() {
		console.log(this.customer)
		this.storefrontService.customerRecover(this.customer).subscribe(
			(data: any) => {
				console.groupCollapsed('data', data)
				if (data.data.customerRecover.customerUserErrors.length > 0) {
					this.success = false
					this.error = data.data.customerRecover.customerUserErrors[0].message
				} else {
					this.success = true
				}
			},
			err => {
				this.error = err.message
			}
		)
	}
}
