import { Component, ViewChild } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import gql from 'graphql-tag'
import { AccountService, Order, StorefrontService, TemplateService } from 'ngx-shopify'

@Component({
	selector: 'component-orders',
	templateUrl: './orders.component.html',
	styleUrls: ['./orders.component.scss'],
})
export class ComponentOrders {
	@ViewChild('selectCountry') selectCountry
	customer: any
	orders: Order[]
	showDetalis: boolean
	store
	loader
	// showTrackingModal:  boolean
	// selectedOrder: string
	constructor(private route: ActivatedRoute, private storefrontService: StorefrontService, private accountService: AccountService, templateService: TemplateService) {
		this.store = templateService.store
		// this.showTrackingModal= false
		this.setup()
	}

	setup() {
		this.storefrontService
			.runGraphqlQuery(
				gql`
					query customerDetails($customerAccessToken: String!) {
						customer(customerAccessToken: $customerAccessToken) {
							email
							firstName
							lastName
							defaultAddress {
								address1
								address2
								city
								company
								country
								countryCode
								firstName
								formatted
								id
								lastName
								latitude
								longitude
								name
								phone
								province
								provinceCode
								zip
							}
							addresses(first: 5) {
								edges {
									node {
										address1
										address2
										city
										company
										country
										countryCode
										firstName
										formatted
										id
										lastName
										latitude
										longitude
										name
										phone
										province
										provinceCode
										zip
									}
								}
							}
							orders(first: 30) {
								edges {
									node {
										currencyCode
										customerLocale
										email
										id
										lineItems(first: 30) {
											edges {
												node {
													customAttributes {
														value
														key
													}
													discountAllocations {
														discountApplication {
															allocationMethod
															targetType
															targetSelection
															value
														}
														allocatedAmount {
															amount
															currencyCode
														}
													}
													quantity
													title
													variant {
														available
														availableForSale
														compareAtPriceV2 {
															currencyCode
															amount
														}
														compareAtPrice
														id
														price
														title
													}
												}
											}
										}
										name
										orderNumber
										phone
										processedAt
										shippingAddress {
											address1
											address2
											city
											company
											country
											countryCode
											firstName
											countryCodeV2
											formatted
											formattedArea
											lastName
											id
											name
											phone
											province
											provinceCode
											zip
										}
										totalShippingPriceV2 {
											currencyCode
											amount
										}
										totalTaxV2 {
											currencyCode
											amount
										}
										totalRefundedV2 {
											currencyCode
											amount
										}
										statusUrl
										totalPriceV2 {
											currencyCode
											amount
										}
										successfulFulfillments(first: 10) {
											trackingCompany
											fulfillmentLineItems(first: 10) {
												edges {
													node {
														lineItem {
															variant {
																title
															}
															title
															quantity
														}
														quantity
													}
												}
											}
										}
									}
								}
								pageInfo {
									hasPreviousPage
									hasNextPage
								}
							}
						}
					}
				`,
				{
					customerAccessToken: this.storefrontService.customerAccessToken(),
				}
			)
			.subscribe((response: any) => {
				this.orders = response.data.customer.orders.edges.reverse()
				console.log('this.orders', this.orders)
			})
	}

	// openTrackingModal(order) {

	//   this.showTrackingModal = false
	//   this.selectedOrder = ''
	//   this.selectedOrder = order.node.id
	//   this.showTrackingModal = true
	// }
}
