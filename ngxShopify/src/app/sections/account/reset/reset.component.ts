import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { StorefrontService } from 'ngx-shopify'

@Component({
	selector: 'component-reset',
	templateUrl: './reset.component.html',
	styleUrls: ['./reset.component.scss'],
})
export class ComponentReset {
	customerPassword
	constructor(private storefrontService: StorefrontService, private router: Router, private route: ActivatedRoute) {}
	async submit() {
		this.storefrontService.customerResetByUrl(window.location.href, this.customerPassword).subscribe(data => {
			console.log('data', data)
			this.router.navigate(['/'])
		})
	}
}
