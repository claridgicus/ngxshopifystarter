import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AccountService } from 'ngx-shopify'

@Component({
	selector: 'component-account-header',
	templateUrl: './account-header.component.html',
	styleUrls: ['./account-header.component.scss'],
})
export class ComponentAccountHeader {
	currentValue
	items
	title
	constructor(private router: Router, private accountService: AccountService) {
		this.checkURL()

		this.items = [
			{
				baseUrl: '/account',
				value: 'Dashboard',
			},
			{
				baseUrl: '/account/info',
				value: 'Account Information',
			},
			{
				baseUrl: '/account/addresses',
				value: 'Addresses',
			},
			{
				baseUrl: '/account/orders',
				value: 'My Orders',
			},
			{
				baseUrl: '/',
				value: 'Logout',
			},
		]
	}

	setCurrentValue(title) {
		this.currentValue = title
		if (title === 'Logout') {
			this.logout()
		}
	}

	selectUrl(event) {
		this.router.navigate([event.baseUrl])
		this.setCurrentValue(event.value)
	}

	logout() {
		this.accountService.logout()
		this.router.navigate(['/'])
	}

	checkURL() {
		let url = this.router.url
		if (url === '/pages/wishlist') {
			this.currentValue = 'Wishlist'
			this.title = 'Wishlist'
		} else if (url === '/account/addresses') {
			this.currentValue = 'Addresses'
			this.title = 'My Account'
		} else if (url === '/account/orders') {
			this.currentValue = 'My Orders'
			this.title = 'My Account'
		} else if (url === '/account/info') {
			this.currentValue = 'Account Information'
			this.title = 'My Account'
		}
	}
}
