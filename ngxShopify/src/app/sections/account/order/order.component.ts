import { Component, ViewChild } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import gql from 'graphql-tag'
import { AccountService, StorefrontService, TemplateService } from 'ngx-shopify'

@Component({
  selector: 'component-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class ComponentOrder {
  @ViewChild('selectCountry') selectCountry
  shippingAddress: NgxShopifyGraphqlOrderDetails.ShippingAddress
  billingAddress: NgxShopifyGraphqlOrderDetails.BillingAddress
  temporaryOrder: NgxShopifyGraphqlOrderDetails.RootObject;
  billingCheck: boolean
  loading: boolean
  showDetalis: boolean
  total: number
  orderId
  store
  loader
  // Todo - handle discounts and giftcards appropriately

  constructor(private route: ActivatedRoute, private storefrontService: StorefrontService, private accountService: AccountService, private templateService: TemplateService) {
    this.store = templateService.store
    this.loader = this.store.shop.cmLoader
    this.loading = true
    this.setup()
  }

  setup() {
    this.route.params.subscribe((params: Params) => {
      this.storefrontService
        .runGraphqlQuery(
          query,
          {
            id: params.id
          }
        )
        .subscribe((orderResponse: any) => {
          if(typeof orderResponse.data === 'undefined'){
            this.billingCheck = false
            console.log("Order does not contain billing adddress ")
          }else{
            this.billingCheck = true
            this.temporaryOrder = orderResponse.data.node
            console.log(orderResponse.data)
            this.shippingAddress = this.temporaryOrder.shippingAddress
            this.total = parseInt(this.temporaryOrder.subtotalPrice)
            this.total += this.temporaryOrder.totalTax ? parseInt(this.temporaryOrder.totalTax) : 0
            this.total += this.temporaryOrder.totalShippingPrice ? parseInt(this.temporaryOrder.totalShippingPrice) : 0
          }
          
          // this.storefrontService.runGraphqlQuery(query,this.temporaryOrder.id).subscribe(
          //   (lineItems: ) => {
          //   }
          // )
          // this.storefrontService.runGraphqlQuery(query,this.temporaryOrder.id).subscribe(
          //   (billingAddress: NgxShopifyGraphqlOrderDetails.ShippingAddress) => {
          //     this.billingAddress = billingAddress
          //   }
          // )
        })

        if (!this.billingCheck){
          this.storefrontService
          .runGraphqlQuery(
            queryWithOutBAddress,
            {
              id: params.id
            }
          )
          .subscribe((orderResponse : any) => {
            console.log("Order without billing loaded")
            console.log(this.temporaryOrder)
            this.temporaryOrder = orderResponse.data.node
            this.shippingAddress = this.temporaryOrder.shippingAddress
            
            this.total = parseInt(this.temporaryOrder.subtotalPrice)
            this.total += this.temporaryOrder.totalTax ? parseInt(this.temporaryOrder.totalTax) : 0
            this.total += this.temporaryOrder.totalShippingPrice ? parseInt(this.temporaryOrder.totalShippingPrice) : 0
          })        
        }
    })
  }


}

let queryWithOutBAddress = gql`
  query orderDetails($id: ID!) {
    node(id: $id) {
      ...on Order {
        id
        orderNumber
        shippingAddress {
          address1
          address2
          city
          company
          country
          countryCode
          firstName
          countryCodeV2
          formatted
          formattedArea
          lastName
          id
          name
          phone
          province
          provinceCode
          zip
        }
        lineItems(first: 10) {
          edges {
            node {
              quantity
              title 
              variant {
                  available
                  availableForSale
                  compareAtPriceV2 {
                    currencyCode
                    amount
                  }
                  compareAtPrice
                  id
                  price
                  title
                  sku
                }  
            }
          }
        }
        totalPrice
        totalShippingPrice
        totalTax
        subtotalPrice
        }
      }
    }

`


let query = gql`
  query orderDetails($id: ID!) {
    node(id: $id) {
      ...on Order {
        id
        orderNumber
        shippingAddress {
          address1
          address2
          city
          company
          country
          countryCode
          firstName
          countryCodeV2
          formatted
          formattedArea
          lastName
          id
          name
          phone
          province
          provinceCode
          zip
        }
        billingAddress {
          address1
          address2
          city
          company
          country
          countryCode     
          countryCodeV2
          formatted
          formattedArea
          id      
          phone
          province
          provinceCode
          zip
        }
        lineItems(first: 10) {
          edges {
            node {
              quantity
              title 
              variant {
                  available
                  availableForSale
                  compareAtPriceV2 {
                    currencyCode
                    amount
                  }
                  compareAtPrice
                  id
                  price
                  title
                  sku
                }  
            }
          }
        }
        totalPrice
        totalShippingPrice
        totalTax
        subtotalPrice
        }
      }
    }

`




declare module NgxShopifyGraphqlOrderDetails {


  export interface BillingAddress {
    address1: string;
    address2: string;
    city: string;
    company?: any;
    country: string;
    countryCode: string;
    countryCodeV2: string;
    formatted: string[];
    formattedArea: string;
    id: string;
    phone: string;
    province: string;
    provinceCode: string;
    zip: string;
}

  export interface ShippingAddress {
      address1: string;
      address2: string;
      city: string;
      company?: any;
      country: string;
      countryCode: string;
      firstName: string;
      countryCodeV2: string;
      formatted: string[];
      formattedArea: string;
      lastName: string;
      id: string;
      name: string;
      phone: string;
      province: string;
      provinceCode: string;
      zip: string;
  }

  export interface Variant {
      available: boolean;
      availableForSale: boolean;
      compareAtPriceV2?: any;
      compareAtPrice?: any;
      id: string;
      price: string;
      title: string;
      sku: string;
  }

  export interface Node {
      quantity: number;
      title: string;
      variant: Variant;
  }

  export interface Edge {
      node: Node;
  }

  export interface LineItems {
      edges: Edge[];
  }

  export interface RootObject {
      id: string;
      orderNumber: number;
      shippingAddress: ShippingAddress;
      billingAddress: BillingAddress;
      lineItems: LineItems;
      totalPrice: string;
      totalShippingPrice: string;
      totalTax: string;
      subtotalPrice: string;
  }

}

