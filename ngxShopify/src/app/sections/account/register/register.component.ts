import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { CustomerCreateInput, StorefrontService } from 'ngx-shopify'

@Component({
  selector: 'component-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class ComponentRegister {
  customer: CustomerCreateInput = new CustomerCreateInput()
  error: any
  errorMessage: string
  loading: boolean = false
  success: boolean = false
  constructor(private storefrontService: StorefrontService, private router: Router) {}

  async submit() {
    this.loading = true
    this.storefrontService.customerCreate(this.customer).subscribe((data: any) => {
      this.loading = false
      if (data.data.customerCreate.userErrors.length > 0) {
        this.errorMessage = data.data.customerCreate.userErrors[0].message
      } else if (data.data.customerCreate.customer) {
        localStorage.setItem('userId', data.data.customerCreate.customer.id)
        this.success = true
      }
    })
  }
}
