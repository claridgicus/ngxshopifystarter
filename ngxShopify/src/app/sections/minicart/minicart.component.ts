import { AfterViewChecked, Component, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core'
import { fade } from 'app/animations'
import { Cart, CartItem, CartService } from 'ngx-shopify'

@Component({
	selector: 'component-minicart',
	templateUrl: './minicart.component.html',
	styleUrls: ['./minicart.component.scss'],
	animations: [fade],
})
export class ComponentMinicart implements OnInit, AfterViewChecked {
	@ViewChild('overlay') overlay
	@ViewChild('minicart') minicart
	showMiniCart: boolean
	cart: Cart
	loading: boolean = false
	display: boolean = false
	compareAtPrice: {
		variantId: number
		compareAtPrice: number | null
	}
	cartItemsCompareAtPrice = []

	constructor(private cartService: CartService, private renderer: Renderer2) {}

	ngAfterViewChecked() {
		if (this.overlay && this.minicart.nativeElement) {
			this.renderer.setStyle(this.minicart.nativeElement, 'top', document.getElementById('cartcount').getBoundingClientRect().bottom + 'px')
		}
	}

	async removeItem(item: CartItem) {
		await this.cartService.updateQuantity(item.id, 0)
	}

	ngOnInit() {
		this.cartService.cartloading.subscribe((state: boolean) => {
			this.loading = state
		})
		this.cartService.cartshown.subscribe((state: boolean) => {
			this.showMiniCart = state
			this.display = state
		})
		this.cartService.cart.subscribe((cart: any) => {
			this.cart = cart
		})
	}

	hideMiniCart() {
		this.cartService.hideMiniCart()
	}

	@HostListener('window:scroll')
	scroll() {
		if (!this.cart.items.length && this.display) {
			setTimeout(() => {
				this.hideMiniCart()
			}, 1000)
		}
	}
}
