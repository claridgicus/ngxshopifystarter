// TODO -> Make a copy for called environment.prod.ts
export const environment = {
	apiurl: '/',
	production: true,
	minicart: false,
	cartCount: {
		dot: false,
		counter: true,
	},
	debugHttp: true,
	instagram: '',
	defaultImg: 'https://via.placeholder.com/250x340',
	storefrontKey: 'YOURSTOREFRONTKEY',
	externalScripts: [
		{
			name: 'googlemaps',
			src: 'https://maps.googleapis.com/maps/api/js?key=YOURKEYFORGOOGLEMAPSJSAPI',
		},
		{
			name: 'okendo',
			src: 'https://d3hw6dc1ow8pp2.cloudfront.net/reviewsWidget.min.js',
		},
	],
}
