// TODO -> Make a copy for local environment.ts
export const environment = {
	apiurl: 'https://YOURSTORE.myshopify.com/',
	production: false,
	minicart: false,
	cartCount: {
		dot: false,
		counter: true,
	},
	debugHttp: true,
	instagram: '',
	defaultImg: 'https://via.placeholder.com/250x340',
	storefrontKey: 'YOURSTOREFRONTKEY',
	externalScripts: [
		{
			name: 'script',
			src: 'fullurltoscript',
		},
	],
}
